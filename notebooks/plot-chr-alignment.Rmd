---
title: "PHG Synteny Plots"
output:
  word_document: default
  html_document:
    df_print: paged
  pdf_document: default
---

```{r setup, echo=FALSE}
library(tidyverse)
library(RPostgreSQL)
```

```{r userid, echo=FALSE}
dbuser = "phgdev"
dbpwd = "D3v3l0p3r"

```

Get the reference range start and end points with the assembly start and end points from the maize1 database.
```{r genotypes-fromdb, echo=FALSE}
drv <- dbDriver("PostgreSQL")
conn <- DBI::dbConnect(drv = drv, host='128.84.180.206', port='5432', dbname='maize_1_0', user=dbuser, password=dbpwd)

query <- "SELECT line_name, chrom, range_start, range_end, asm_contig, asm_start_coordinate, asm_end_coordinate FROM haplotypes, reference_ranges, genotypes, gamete_haplotypes, gametes WHERE haplotypes.ref_range_id=reference_ranges.ref_range_id AND haplotypes.gamete_grp_id=gamete_haplotypes.gamete_grp_id AND gamete_haplotypes.gameteid=gametes.gameteid AND gametes.genoid=genotypes.genoid AND haplotypes.method_id=4 AND line_name = 'Oh43_Assembly' order by chrom, range_start"

assemblyDataOh43 <- dbGetQuery(conn, query)

query <- "SELECT line_name, chrom, range_start, range_end, asm_contig, asm_start_coordinate, asm_end_coordinate FROM haplotypes, reference_ranges, genotypes, gamete_haplotypes, gametes WHERE haplotypes.ref_range_id=reference_ranges.ref_range_id AND haplotypes.gamete_grp_id=gamete_haplotypes.gamete_grp_id AND gamete_haplotypes.gameteid=gametes.gameteid AND gametes.genoid=genotypes.genoid AND haplotypes.method_id=4 AND line_name = 'CML247_Assembly' order by chrom, range_start"

assemblyDataCML247 <- dbGetQuery(conn, query)

dbDisconnect(conn = conn)
```

Plot B73 Reference Range start sites vs Oh43 assembly start coordinates.
```{r plot-Oh43, echo=FALSE}

for (chr in 1:10) {
  chrData <- assemblyDataOh43 %>% filter(chrom==as.character(chr))
  print(ggplot(data = chrData, mapping = aes(x=range_start, y=asm_start_coordinate)) + 
    geom_point(shape=".") + xlab("B73 coordinates") +ylab("Oh43 coordinates") + 
    ggtitle(paste("Chromosome", chr)))
}

```
Plot B73 Reference Range start sites vs CML247 assembly start coordinates.
```{r plot-CML247, echo=FALSE}
for (chr in 1:10) {
  chrData <- assemblyDataCML247 %>% filter(chrom==as.character(chr))
  print(ggplot(data = chrData, mapping = aes(x=range_start, y=asm_start_coordinate)) + 
    geom_point(shape=".") + xlab("B73 coordinates") +ylab("CML247 coordinates") + 
    ggtitle(paste("Chromosome", chr)))
}

```

