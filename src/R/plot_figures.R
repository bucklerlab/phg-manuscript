#!/usr/bin/env Rscript

#--------------------------------------------------------------------
# Script Name:   plot_generation.R
# Description:   Generate figures for the PHG manuscript
# Author:        Brandon Monier
# Created:       2021-06-02 at 11:30:50
# Last Modified: 2021-06-02 at 11:31:10
#--------------------------------------------------------------------

#--------------------------------------------------------------------
# Detailed Purpose:
#    The main purpose of this Rscript is to generate bar charts for
#    the PHG manuscript and save high resolution outputs to disk.
#--------------------------------------------------------------------

# === Preamble ======================================================

library(tidyverse)
library(jsonlite)
library(mdthemes)
library(ggplot2)

## Color palette (CB-friendly) ----
## https://jfly.uni-koeln.de/color/
cbp1 <- c(
    "#E69F00", "#56B4E9", "#009E73",
    "#F0E442", "#0072B2", "#D55E00",
    "#CC79A7", "#999999"
)

all.methods <- read_csv(file = "local_data/method_table.csv")
smry <- read_tsv("local_data/mapping_summary.txt")


# === Data munging ==================================================


# create separate data frames for consensus, read mappings, and paths then parse the descriptions
consensus_methods <- all.methods %>% filter(grepl("^CONSEN",name))
tmp1 <- lapply(consensus_methods$description,fromJSON)
tmp2 <- do.call(rbind, lapply(tmp1, unlist))
consensus_methods <- cbind(consensus_methods, tmp2)

read_methods <- all.methods %>% filter(grepl("^ReadMap",name))
tmp1 <- lapply(read_methods$description,fromJSON)
tmp2 <- do.call(rbind, lapply(tmp1, unlist))
read_methods <- cbind(read_methods, tmp2)

path_methods <- all.methods %>% filter(grepl("^Path",name))
tmp1 <- lapply(path_methods$description,fromJSON)
tmp2 <- do.call(rbind, lapply(tmp1, unlist))
path_methods <- cbind(path_methods, tmp2)
rm(tmp1,tmp2)

smry$pCorrect <- smry$nCorrect/smry$nReads
smry

hapMethods <- consensus_methods %>% select(name, mxDiv, clusteringMode, maxClusters)

# add mummer4
smry <- smry %>% left_join(hapMethods, by = c("haplotypeMethod" = "name")) %>%
    mutate(readType = ifelse(paired,"paired","single"))


## Coding for fParameters ----
fParms <- c("a","b","c","d","e","f")
names(fParms) <- c("f1000,5000","f5000,6000","f10000,11000","f15000,16000","f20000,21000","f25000,26000")


## FIGURE 5 ----
selectSmry <- smry %>%
    filter(fParam=="f1000,5000") %>%
    mutate(
        maxDv = format(mxDiv, scientific = T, na.encode = T),
        mode = ifelse(
            is.na(clusteringMode), "kmer_distance",
            ifelse(clusteringMode == "kmer_assembly", "kmer_distance", "SNP_distance")
        ),
        lineName = sub("_.+","",lineName)
    )

selectSmry$mode <- gsub("kmer_distance", "*k*-mer distance", selectSmry$mode)
selectSmry$mode <- gsub("_", " ", selectSmry$mode)
selectSmry$mode <- gsub("original", "Original", selectSmry$mode)
selectSmry$readType <- gsub("single", "Single", selectSmry$readType)
selectSmry$readType <- gsub("paired", "Paired", selectSmry$readType)

p <- ggplot(selectSmry) +
    geom_col(mapping = aes(x = as.factor(maxDv), y = 1 - pCorrect, fill = lineName), position = 'dodge') +
    facet_wrap(~ readType + mode, nrow = 2) +
    scale_fill_manual(values = cbp1) +
    guides(fill = guide_legend(title = "**Line Name**")) +
    xlab("Diversity Cutoff") +
    ylab("Read Mapping Error Rate") +
    theme(
        legend.text = element_text(size = rel(1.0)),
        strip.text = element_text(size = rel(0.9))
    )

pF5 <- p +
    mdthemes::md_theme_bw() +
    theme(legend.position = "bottom")



## FIGURE 7 ----
selectSmry <- smry %>%
    filter(mxDiv=="1.0E-4" | is.na(mxDiv)) %>%
    mutate(
        maxDv = format(mxDiv, scientific = T, na.encode = T),
        mode = ifelse(
            is.na(clusteringMode), "original",
            ifelse(clusteringMode == "kmer_assembly", "kmer_distance", "SNP_distance")
        ),
        lineName = sub("_.+","",lineName)
    )

selectSmry$mode <- gsub("kmer_distance", "*k*-mer distance", selectSmry$mode)
selectSmry$mode <- gsub("_", " ", selectSmry$mode)
selectSmry$mode <- gsub("original", "Original", selectSmry$mode)
selectSmry$readType <- gsub("single", "Single", selectSmry$readType)
selectSmry$readType <- gsub("paired", "Paired", selectSmry$readType)

p <- ggplot(selectSmry) +
    geom_col(mapping = aes(x = fParms[fParam], y = 1 - pCorrect, fill = lineName), position = 'dodge') +
    facet_wrap(~ readType + mode, nrow = 2) +
    scale_fill_manual(values = cbp1) +
    #ggtitle("All Reference Ranges") +
    guides(fill = guide_legend(title = "**Line Name**")) +
    xlab("Minimap *fparam* Value") +
    ylab("Read Mapping Error Rate") +
    theme(
        legend.text = element_text(size = rel(1.0)),
        strip.text = element_text(size = rel(0.9))
    )

pF7 <- p +
    mdthemes::md_theme_bw() +
    theme(legend.position = "bottom")



smry_genic <- read_tsv("local_data/mapping_summary_refRegionGroup.txt") %>%
    left_join(hapMethods, by = c("haplotypeMethod" = "name"))
smry_genic <- smry_genic %>%
    mutate(
        pCorrect = nCorrect /nReads,
        maxDv = format(mxDiv, scientific = T, na.encode = T),
        mode = ifelse(is.na(clusteringMode), "kmer_assembly", levels(clusteringMode)[clusteringMode]),
        readType = ifelse(paired, "paired", "single")
    )

# coding for fParameters
fParms <- c("a","b","c","d","e","f")
names(fParms) <- c("f1000,5000","f5000,6000","f10000,11000","f15000,16000","f20000,21000","f25000,26000")

selectSmry <- smry_genic %>%
    filter(((maxClusters==30 & mxDiv!=5e-2) | is.na(maxClusters) & fParam=="f1000,5000")) %>%
    mutate(
        maxDv = format(mxDiv, scientific = T, na.encode = T),
        mode = ifelse(
            is.na(clusteringMode), "kmer_distance",
            ifelse(clusteringMode == "kmer_assembly", "kmer_distance", "SNP_distance")
        ),
        lineName = sub("_.+","",lineName)
    )
selectSmry$mode <- gsub("kmer_distance", "*k*-mer distance", selectSmry$mode)
selectSmry$mode <- gsub("_", " ", selectSmry$mode)
selectSmry$mode <- gsub("original", "Original", selectSmry$mode)
selectSmry$readType <- gsub("single", "Single", selectSmry$readType)
selectSmry$readType <- gsub("paired", "Paired", selectSmry$readType)

ggplot(selectSmry) +
    geom_col(mapping = aes(x=as.factor(maxDv), y = 1 - pCorrect, fill = lineName), position = 'dodge') +
    facet_wrap(~ readType + mode, nrow = 2) +
    #ggtitle("Genic Reference Ranges") +
    xlab("Diversity Cutoff") +
    ylab("Read Mapping Error Rate") +
    theme(legend.text=element_text(size=rel(1.0)), strip.text=element_text(size=rel(0.9)) )




selectSmry <- smry_genic %>% filter(mxDiv=="1.0E-4" | is.na(mxDiv)) %>%
    mutate(maxDv = format(mxDiv, scientific = T, na.encode = T), mode=ifelse(is.na(clusteringMode), "original", ifelse(clusteringMode=="kmer_assembly", "kmer_distance", "SNP_distance")),  lineName = sub("_.+","",lineName))

ggplot(selectSmry) +
    geom_col(mapping = aes(x=fParms[fParam], y = 1 - pCorrect, fill = lineName), position = 'dodge') +
    facet_wrap(~ readType + mode, nrow = 2) +
    #ggtitle("Genic Reference Ranges") +
    xlab("Minimap fparam Value") +
    ylab("Read MappingError Rate") +
    theme(legend.text=element_text(size=rel(1.0)), strip.text=element_text(size=rel(0.9)) )


