package simulation

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.VariantUtils
import net.maizegenetics.pangenome.api.WriteFastaFromGraphPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.GZipCompression
import net.maizegenetics.pangenome.db_loading.VariantsProcessingUtils
import net.maizegenetics.pangenome.hapCalling.BestHaplotypePathPlugin
import net.maizegenetics.pangenome.hapCalling.FastqToMappingPlugin
import net.maizegenetics.pangenome.hapCalling.SimulatedReadsPlugin
import net.maizegenetics.pangenome.hapCalling.decodeHapIdMapping
import net.maizegenetics.pangenome.hapcollapse.RunHapConsensusPipelinePlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.prefs.TasselPrefs
import org.apache.log4j.BasicConfigurator
import org.apache.log4j.LogManager

import java.io.File
import java.io.PrintWriter
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Paths
import java.sql.Connection
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.ArrayList

/**
 * main method
 * commands to run from docker using the following template:
 * docker1 run -t --rm biohpc_pjb39/simulation <command>
 * commands used for analysis:
 * /tassel-5-standalone/run_anything.pl -Xmx10G -debug simulation.SimulateKt testPaths &> /workdir/pjb39/testlog.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt consensus &> /workdir/pjb39/logs/consensus_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt pangenome &> /workdir/pjb39/logs/pangenome_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt reads &> /workdir/pjb39/logs/reads_1_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt map &> /workdir/pjb39/logs/map_scenario_1_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt mappingSummary /workdir/simulation/output/mapping_summary.txt  &> /workdir/pjb39/logs/map_summary_1_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt findPaths  &> /workdir/pjb39/logs/find_paths_1_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt hapSummary PathMethod1 /workdir/simulation/output/path1Summary.txt > /workdir/pjb39/logs/pathSummary_1_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt mappingByMethodSummary /workdir/simulation/output/mapping_summary &> /workdir/pjb39/logs/mapSummary_byRangeGroup_log.txt
 * /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt pathSummary /workdir/simulation/output/pathSummaryAll.txt > /workdir/pjb39/logs/pathSummaryAll_log.txt
 */
fun main(args : Array<String>) {

    if (args.size == 0) {
        testCompareVariants()
        return
    }

    BasicConfigurator.configure();
    myLogger.info("printing logger test message")

    TasselPrefs.putMaxThreads(40)
    if (!File(configFile).exists()) writeConfigFile()
    ParameterCache.load(configFile)
    if (args[0].equals("consensus")) runConsensus_1()
    else if (args[0].equals("pangenome")) writeAndIndexPangenome()
    else if (args[0].equals("reads")) simulateReads()
    else if (args[0].equals("map")) readMappingScenario1()
    else if (args[0].equals("mapFParam")) readMappingScenarioFParam()
    else if (args[0].equals("mapMaxErr")) readMappingMaxErr()
    else if (args[0].equals("mappingSummary")) writeMappingSummaryResults(args[1])
    else if (args[0].equals("mappingByMethodSummary")) writeMappingSummaryResultsForRefRangeMethods(args[1])
    else if (args[0].equals("testPaths")) testPaths()
    else if (args[0].equals("findPaths")) findPathsScenario1()
    else if (args[0].equals("findPaths2")) findPathsScenario2()
    else if (args[0].equals("pathSummary")) pathMethodSummary(args[1])
    else if (args[0].equals("hapSummary")) haploidPathSummary(args[1], args[2])
    else if (args[0].equals("methodDescriptions")) exportMethodDescriptions(args[1])
    else if (args[0].equals("rewritePathKeyFiles")) rewritePathKeyFilesInFastqDir()
    else if (args[0].equals("validateHapids")) validateHaplotypeCounts(args[1], args[2].toInt())
    else if (args[0].equals("howCompressed")) howCompressedIsIt()
}

//directories where work products will be stored. directories all end in /
private val myLogger = LogManager.getLogger("SimulateKt")
private val baseDir = "/workdir"
private val fastqDir = "${baseDir}/simulation/fastq"
private val pangenomeDir = "${baseDir}/simulation/pangenome"
private val outputDir = "${baseDir}/simulation/output"
private val configFile = "${baseDir}/simulation/config.txt"
private val db = "${baseDir}/simulation/data/phg_v5Assemblies_20200608_simulation.db"
private val referenceFasta = "${baseDir}/data/assemblies/Zm-B73-REFERENCE-NAM-5.0.fa.gz"
private val rankingFile = "${baseDir}/simulation/data/rankingFile.txt"
private val minimapLoc = "minimap2"
private val nonConsensusHaplotypeMethod = "mummer4"

private var coverage = 0.1


private var lineNames = arrayOf("B73_Assembly","CML247_Assembly","Oh43_Assembly")

private fun runConsensus_1() {
    val mxDivList = listOf<Double>(0.001, 0.0001, 0.00001, 0.000001, 0.0000001)
    runConsensus(mxDivList,
        listOf(30),
        listOf(RunHapConsensusPipelinePlugin.CLUSTERING_MODE.upgma_assembly),
        1)
    runConsensus(mxDivList,
        listOf(30),
        listOf(RunHapConsensusPipelinePlugin.CLUSTERING_MODE.kmer_assembly),
        6)
}


private fun runConsensus(
    mxDivValues: List<Double>,
    maxClusterValues: List<Int>,
    clusterModes: List<RunHapConsensusPipelinePlugin.CLUSTERING_MODE>,
    startIndex: Int
) {

    //make sure to pick a start index that has not been used to avoid constraint errors
    //get a list of method names and print an error message if attempting to use an existing one
    val dbConn = DBLoadingUtils.connection(false)
    val methodNameQuery = "SELECT name FROM methods"
    val methodNameRS = dbConn.createStatement().executeQuery(methodNameQuery)
    val methodNameSet = HashSet<String>()
    while (methodNameRS.next()) {
        methodNameSet.add(methodNameRS.getString(1))
    }

    methodNameRS.close()
    dbConn.close()

    //create graph
    val graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .methods("mummer4")
            .includeVariantContexts(true)
            .configFile(configFile)
            .performFunction(null)

    var consensusIndex = startIndex
    for (mode in clusterModes) {
        for (mxDiv in mxDivValues) {
            for (maxClust in maxClusterValues) {
                var collapseMethod = "CONSENSUS$consensusIndex"
                while (methodNameSet.contains(collapseMethod)) {
                    println("The method name ${collapseMethod} has already been added to the database. It will be skipped.")
                    consensusIndex++
                    collapseMethod = "CONSENSUS$consensusIndex"
                }

                myLogger.info("running ${collapseMethod} for mxDiv = ${mxDiv}, maxClust = ${maxClust}")
                val startTime = System.currentTimeMillis()
                RunHapConsensusPipelinePlugin(null, false)
                    .reference(referenceFasta)
                    .dbConfigFile(configFile)
                    .maxNumberOfClusters(maxClust)
                    .mxDiv(mxDiv)
                    .collapseMethod(collapseMethod)
                    .minAlleleFrequency(0.5)
                    .minSiteForComp(30)
                    .rankingFile(rankingFile)
                    .minTaxaCoverage(0.1)
                    .minTaxa(1)
                    .clusteringMode(mode)
                    .performFunction(graphDS)
                consensusIndex++;
                myLogger.info("TIMING: ${collapseMethod} ran in ${System.currentTimeMillis() - startTime} ms.")
            }
        }
    }
}

private fun writeConfigFile() {
    PrintWriter(configFile).use {
        it.println("host=localHost")
        it.println("user=sqlite")
        it.println("password=sqlite")
        it.println("DB=$db")
        it.println("DBtype=sqlite")
    }
}

private fun testPaths() {
     listOf(fastqDir, pangenomeDir, configFile, db, referenceFasta).forEach {
         val msg = if (File(it).exists()) "$it exists." else "$it does not exist!"
         println(msg)
     }
}

private fun writeAndIndexPangenome() {
    val hapMethods = mutableListOf<String>("mummer4")
    (1..10).forEach { hapMethods.add("CONSENSUS$it") }
    hapMethods.forEach { methodName ->
        val fastaFile = "${pangenomeDir}/pangenome_${methodName}.fa"
        if (!File(fastaFile).exists()) {
            val graphDS = HaplotypeGraphBuilderPlugin(null, false)
                .methods(methodName)
                .includeVariantContexts(false)
                .configFile(configFile)
                .performFunction(null)
            myLogger.info("Writing fasta to $fastaFile")
            WriteFastaFromGraphPlugin(null,false)
                .outputFile(fastaFile)
                .performFunction(graphDS)
            val indexFile = "${pangenomeDir}/pangenome_${methodName}.mmi"
            val processBuilder = ProcessBuilder(minimapLoc, "-d", indexFile, "-I90G", "-k21", "-w11", fastaFile)
            processBuilder.redirectError(ProcessBuilder.Redirect.INHERIT)
            processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)
            val myProcess = processBuilder.start()
            val ret = myProcess.waitFor()
            myLogger.info("Finished writing index $indexFile with return value of $ret")
        }
    }
}

private fun simulateReads() {

    val hapMethod = "mummer4"

    val keyfileWriterSingle = PrintWriter("${fastqDir}/keyFile_single.txt")
    val keyfileWriterPaired = PrintWriter("${fastqDir}/keyFile_paired.txt")
    keyfileWriterSingle.println("cultivar\tflowcell_lane\tfilename\tPlateID")
    keyfileWriterPaired.println("cultivar\tflowcell_lane\tfilename\tfilename2\tPlateID")

    val graphDS = HaplotypeGraphBuilderPlugin(null, false)
        .methods(hapMethod)
        .includeVariantContexts(false)
        .configFile(configFile)
        .performFunction(null)

    lineNames.forEach { name ->
        SimulatedReadsPlugin(null, false)
            .coverage(coverage)
            .fastqDir(fastqDir)
            .haplotypeMethod(hapMethod)
            .insertLength(50)
            .lineName(name)
            .readLength(150)
            .pairedEnd(false)
            .performFunction(graphDS)

        val nameSingle = "${name.replace("_Assembly", "")}_single"
        keyfileWriterSingle.println("${nameSingle}\tsingle\t${name}_${hapMethod}_${coverage}X_SR.fq.gz\tsingle")


        SimulatedReadsPlugin(null, false)
            .coverage(coverage)
            .fastqDir(fastqDir)
            .haplotypeMethod(hapMethod)
            .insertLength(50)
            .lineName(name)
            .readLength(150)
            .pairedEnd(true)
            .performFunction(graphDS)

        val namePaired = "${name.replace("_Assembly", "")}_paired"
        keyfileWriterPaired.println("${name}\tpaired\t${name}_${hapMethod}_${coverage}X_PE1.fq.gz\t${name}_${hapMethod}_${coverage}X_PE2.fq.gz\tpaired")
    }
    keyfileWriterSingle.close()
    keyfileWriterPaired.close()
}

data class ReadMappingParameters(val hapMethodList : List<String>,
                                 val maxErrList : List<Double>,
                                 val maxSecondaryList : List<Int>,
                                 val fParamList : List<String>)

private fun mapReads(parameters : ReadMappingParameters) {
    //mapping takes several parameters which may have different values in different scenarios

    var mapIndex = maxMappingIndex() + 1
    for (haplotypeMethod in parameters.hapMethodList) {
        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .methods(haplotypeMethod)
            .includeVariantContexts(false)
            .configFile(configFile)
            .performFunction(null)

        for (err in parameters.maxErrList) {
            for (maxSec in parameters.maxSecondaryList) {
                for (fpar in parameters.fParamList) {
                    val methodName = "ReadMapping$mapIndex"
                    val keyfileNameSingle = "${fastqDir}/keyFile_single.txt"
                    val indexFilename = "${pangenomeDir}/pangenome_${haplotypeMethod}.mmi"

                    FastqToMappingPlugin(null, false)
                        .keyFile(keyfileNameSingle)
                        .maxSecondaryAlignments(maxSec)
                        .maxRefRangeError(err)
                        .indexFile(indexFilename)
                        .fastqDir("${fastqDir}/")
                        .minimapLocation(minimapLoc)
                        .fParameter(fpar)
                        .methodName(methodName)
                        .performFunction(graphDS)

                    val keyfileNamePaired = "${fastqDir}/keyFile_paired.txt"

                    FastqToMappingPlugin(null, false)
                        .keyFile(keyfileNamePaired)
                        .maxSecondaryAlignments(maxSec)
                        .maxRefRangeError(err)
                        .indexFile(indexFilename)
                        .fastqDir("${fastqDir}/")
                        .minimapLocation(minimapLoc)
                        .fParameter(fpar)
                        .methodName(methodName)
                        .performFunction(graphDS)


                    mapIndex++

                    //write pathkeyfile contents to one file
                    val singleKeyFileName = "${fastqDir}/keyFile_single_pathKeyFile.txt"
                    val pairedKeyFilename = "${fastqDir}/keyFile_paired_pathKeyFile.txt"
                    val newPathKeyFilename = "${fastqDir}/keyfile_${haplotypeMethod}_${err}_${maxSec}_${fpar}_pathKeyFile.txt"

                    PrintWriter(newPathKeyFilename).use { pw ->
                        Files.newBufferedReader(Paths.get(singleKeyFileName)).use {
                            it.lines().forEach { pw.println(it) }
                        }
                        Files.newBufferedReader(Paths.get(pairedKeyFilename)).use {
                            it.lines().skip(1).forEach { pw.println(it) }
                        }
                    }

                }
            }
        }
    }
}

private fun maxMappingIndex() : Int {
    val dbconn = DBLoadingUtils.connection(false)
    val sql = "SELECT name FROM methods WHERE name LIKE 'ReadMapping%'"
    val result = dbconn.createStatement().executeQuery(sql)
    val nameList = ArrayList<String>()
    while (result.next()) {
        nameList.add(result.getString(1))
    }
    val maxIndex = nameList.map { it.substring(11).toInt() }.max()

    dbconn.close()
    return maxIndex ?: 0
}

private fun readMappingScenario1() {
    val hapMethods = mutableListOf<String>("mummer4")
    (1..10).forEach { hapMethods.add("CONSENSUS$it") }
    mapReads(ReadMappingParameters(hapMethods,
        maxErrList=listOf(0.25),
        maxSecondaryList= listOf(30),
        fParamList= listOf("f1000,5000")))
}

private fun readMappingScenarioFParam() {
    //test single end reads, fParmam = f10000,11000;f15000,f16000;f20000,21000;
    //for upgma 1e-4 (CONSENSUS2), kmer 1e-4 (CONSENSUS7), mummer4
    val hapMethods = listOf("mummer4", "CONSENSUS2", "CONSENSUS7")
    mapReads(ReadMappingParameters(hapMethods,
        maxErrList=listOf(0.25),
        maxSecondaryList= listOf(30),
        fParamList= listOf("f5000,6000","f10000,11000","f15000,16000","f20000,21000", "f25000,26000")))
}

private fun readMappingMaxErr() {
    //maxRefRangeErr in 0.05, 0.1, 0.25, 0.5 (mummer4 only)
    mapReads(ReadMappingParameters(hapMethodList = listOf("mummer4"),
        maxErrList = listOf(0.05, 0.1, 0.25, 0.5),
        maxSecondaryList = listOf(30),
        fParamList = listOf("f1000,5000")))
}

private fun writeMappingSummaryResults(filename : String) {
    val summary = summarizeMappingResults()
    PrintWriter(filename).use { pw ->
        pw.println("lineName\treadMethod\tpaired\thaplotypeMethod\tmaxErr\tmaxSecondary\tfParam\tnReads\tnCorrect")
        summary.forEach { pw.println("${it.lineName}\t${it.readMethod}\t${it.paired}\t${it.haplotypeMethod}\t${it.maxErr}\t${it.maxSecondary}\t${it.fParam}\t${it.numberOfReads}\t${it.numberCorrect}") }
    }

}

private fun writeMappingSummaryResultsForRefRangeMethods(filename : String) {
    //methodId/type/name
    //2|7|refRegionGroup
    //3|7|refInterRegionGroup

    val filenameRegion = "${filename}_refRegionGroup.txt"
    val summary = summarizeMappingResultsByMethod(2)
    PrintWriter(filenameRegion).use { pw ->
        pw.println("group\tlineName\treadMethod\tpaired\thaplotypeMethod\tmaxErr\tmaxSecondary\tfParam\tnReads\tnCorrect")
        summary.forEach { pw.println("refRegionGroup\t${it.lineName}\t${it.readMethod}\t${it.paired}\t${it.haplotypeMethod}\t${it.maxErr}\t${it.maxSecondary}\t${it.fParam}\t${it.numberOfReads}\t${it.numberCorrect}") }
    }

    //uncomment to get interregion group
//    val filenameInterRegion = "${filename}_refInterRegionGroup.txt"
//    val summary2 = summarizeMappingResultsByMethod(3)
//    PrintWriter(filenameInterRegion).use { pw ->
//        pw.println("group\tlineName\treadMethod\tpaired\thaplotypeMethod\tmaxErr\tmaxSecondary\tfParam\tnReads\tnCorrect")
//        summary2.forEach { pw.println("refInterRegionGroup\t${it.lineName}\t${it.readMethod}\t${it.paired}\t${it.haplotypeMethod}\t${it.maxErr}\t${it.maxSecondary}\t${it.fParam}\t${it.numberOfReads}\t${it.numberCorrect}") }
//    }
}

private fun summarizeMappingResults1() : List<ReadMappingSummary> {
    //for each read mapping calculate number of reads mapped and number of reads mapped correctly
    val conn = DBLoadingUtils.connection(false)

    //get contents of the read mapping table
    val sqlMappings = "SELECT read_mapping_id, genoid, methods.method_id, name, file_group_name FROM read_mapping, methods " +
            "WHERE read_mapping.method_id=methods.method_id"
    val mappingInfo = ArrayList<ReadMappingInfo>()
    val resultMappings = conn.createStatement().executeQuery(sqlMappings)
    println("executing query: $sqlMappings")
    while (resultMappings.next()) {
        mappingInfo.add(ReadMappingInfo(resultMappings.getInt(1), resultMappings.getInt(2),
            resultMappings.getInt(3), resultMappings.getString(4), resultMappings.getString(5)))
    }
    resultMappings.close()

    //which genoids have mummer4 haplotypes? Get the genoid and line_name. Store those as a list of genoids and a genoid -> name map.
    val sqlGenoid = "SELECT DISTINCT genotypes.genoid, line_name FROM genotypes, gamete_haplotypes, gametes " +
            "WHERE gamete_haplotypes.gameteid=gametes.gameteid AND gametes.genoid=genotypes.genoid " +
            "AND gamete_haplotypes.gamete_grp_id in (SELECT DISTINCT gamete_grp_id " +
            "FROM haplotypes, methods WHERE haplotypes.method_id = methods.method_id AND methods.name='${nonConsensusHaplotypeMethod}')"
    val genoidList = ArrayList<Int>()
    val genoidNameMap = HashMap<Int, String>()
    val resultGenoid = conn.createStatement().executeQuery(sqlGenoid)
    while(resultGenoid.next()) {
        genoidList.add(resultGenoid.getInt(1))
        genoidNameMap.put(resultGenoid.getInt(1), resultGenoid.getString(2))
    }
    resultGenoid.close()

    //is the read mapping for one of the taxa that has non-consensus haplotypes (mummer4)?
    val mummer4Mappings = mappingInfo.filter { genoidList.contains(it.genoId) }
    val summaryList = ArrayList<ReadMappingSummary>()

    //after filtering the set of reads mappings to those with mummer4 haplotypes, summarize the results
    mummer4Mappings.forEach {
        //get the read mapping
        val sqlMapping = "SELECT mapping_data FROM read_mapping WHERE read_mapping_id = ${it.readId}"
        val resultMapping = conn.createStatement().executeQuery(sqlMapping)

        //count total and correctly mapped reads
        if (resultMapping.next()) {
            //decompress the read mapping to a Map<List<Int>,Int>
            val detailMap = getMethodDetails(conn, it.methodId)
            val pangenomeIndex = detailMap?.get("minimap2IndexFile")
            val haplotypeMethod = if (pangenomeIndex == null) "NA" else indexFileToHaplotypeMethod(pangenomeIndex)
            val hapidSet = getHapidSetForTarget(conn, it.genoId, haplotypeMethod)
            val hits = decodeHapIdMapping(resultMapping.getBytes(1))
            val numberOfReads = hits.values.sum()
            val numberOfCorrectReads = hits.entries.filter { entry ->
                val hapids = entry.key
                //does one of the hapids match the target?
                hapids.any { hapidSet.contains(it) }
            }.map { it.value }.sum()
            val maxErr = detailMap?.get("maxRefRangeErr")?.toDouble() ?: -1.0
            val maxSec = detailMap?.get("maxSecondary")?.toInt() ?: -1
            val fParam = detailMap?.get("fParameter") ?: "NA"
            val lineName = genoidNameMap[it.genoId] ?: "NA"
            val readMethod = it.methodName
            val paired = it.fileGroup.contains("paired")
            summaryList.add(ReadMappingSummary(lineName, readMethod, haplotypeMethod, maxErr, maxSec, fParam, paired, numberOfReads, numberOfCorrectReads))
        } else myLogger.error("no mapping for read_mapping_id = ${it.readId}")
    }

    conn.close()
    return summaryList
}

private fun summarizeMappingResults() : List<ReadMappingSummary> {

    //for each read mapping calculate number of reads mapped and number of reads mapped correctly
    val conn = DBLoadingUtils.connection(false)

    //make map or target genotype ids
    val genoidMap = HashMap<String, Int>()

    val sqlGeno ="SELECT genoid, line_name FROM genotypes"
    println("Running query: $sqlGeno")
    val resultGeno = conn.createStatement().executeQuery(sqlGeno)
    while (resultGeno.next()) {
        genoidMap.put(resultGeno.getString("line_name"), resultGeno.getInt("genoid"))
    }
    resultGeno.close()

    val lineNameMap = genoidMap.entries.map { Pair(it.value, it.key) }.toMap()

    val targetMap = HashMap<Int,Int>()
    targetMap.put(genoidMap["B73_single"] ?: 0, genoidMap["B73_Assembly"] ?: 0)
    targetMap.put(genoidMap["B73_paired"] ?: 0, genoidMap["B73_Assembly"] ?: 0)
    targetMap.put(genoidMap["CML247_single"] ?: 0, genoidMap["CML247_Assembly"] ?: 0)
    targetMap.put(genoidMap["CML247_paired"] ?: 0, genoidMap["CML247_Assembly"] ?: 0)
    targetMap.put(genoidMap["Oh43_single"] ?: 0, genoidMap["Oh43_Assembly"] ?: 0)
    targetMap.put(genoidMap["Oh43_paired"] ?: 0, genoidMap["Oh43_Assembly"] ?: 0)

    //method hash
    val methodMaps = HashMap<Int, Map<String,String>>()
    val sqlMethod = "SELECT method_id, name, description FROM methods WHERE name like 'Read%'"
    println("Running query: $sqlMethod")
    val resultMethod = conn.createStatement().executeQuery(sqlMethod)
    while (resultMethod.next()) {
        val detailMap = DBLoadingUtils.parseMethodJsonParamsToString(resultMethod.getString(3))
        val deqoutedMap = detailMap.entries.map { Pair(it.key.replace("\"",""), it.value.replace("\"","")) }.toMap()
        methodMaps.put(resultMethod.getInt(1), deqoutedMap)
    }
    resultMethod.close()

    //get readMappings
    val summaryList = ArrayList<ReadMappingSummary>()
    val sqlReads = "SELECT read_mapping_id, genoid, method_id, file_group_name, mapping_data FROM read_mapping"
    println("Running query: $sqlReads")
    val resultReads = conn.createStatement().executeQuery(sqlReads)
    while (resultReads.next()) {
        //decompress the read mapping to a Map<List<Int>,Int>
        val detailMap = methodMaps[resultReads.getInt("method_id")]
        val pangenomeIndex = if (detailMap != null) detailMap["minimap2IndexFile"] else null
        val haplotypeMethod = if (pangenomeIndex == null) "NA" else indexFileToHaplotypeMethod(pangenomeIndex)
        val readGenoid = resultReads.getInt("genoid")
        val targetGenoid = targetMap[readGenoid]
        check(targetGenoid != null) {"target genoid is null for genoid = ${resultReads.getInt("genoid")}"}
        val hapidSet = getHapidSetForTarget(conn, targetGenoid, haplotypeMethod)
        val hits = decodeHapIdMapping(resultReads.getBytes("mapping_data"))
        val numberOfReads = hits.values.sum()
        val numberOfCorrectReads = hits.entries.filter { entry ->
            val hapids = entry.key
            //does one of the hapids match the target?
            hapids.any { hapidSet.contains(it) }
        }.map { it.value }.sum()
        val maxErr = detailMap?.get("maxRefRangeErr")?.toDouble() ?: -1.0
        val maxSec = detailMap?.get("maxSecondary")?.toInt() ?: -1
        val fParam = detailMap?.get("fParameter") ?: "NA"
        val lineName = lineNameMap[readGenoid] ?: "NA"
        val readMethod = detailMap?.get("methodName") ?: "NA"
        val paired = lineName.endsWith("paired")
        summaryList.add(ReadMappingSummary(lineName, readMethod, haplotypeMethod, maxErr, maxSec, fParam, paired, numberOfReads, numberOfCorrectReads))

    }

    return summaryList
}

data class ReadMappingInfo(val readId : Int, val genoId : Int, val methodId : Int, val methodName : String, val fileGroup : String)
data class ReadMappingSummary(val lineName : String, val readMethod : String, val haplotypeMethod : String, val maxErr : Double, val maxSecondary : Int,
                              val fParam : String, val paired : Boolean,
                              val numberOfReads : Int, val numberCorrect : Int)

private fun summarizeMappingResultsByMethod(refRangeMethodId : Int) : List<ReadMappingSummary> {

    //for each read mapping calculate number of reads mapped and number of reads mapped correctly
    val conn = DBLoadingUtils.connection(false)

    //make map or target genotype ids
    val genoidMap = HashMap<String, Int>()

    val sqlGeno ="SELECT genoid, line_name FROM genotypes"
    val resultGeno = conn.createStatement().executeQuery(sqlGeno)
    while (resultGeno.next()) {
        genoidMap.put(resultGeno.getString("line_name"), resultGeno.getInt("genoid"))
    }
    resultGeno.close()

    val lineNameMap = genoidMap.entries.map { Pair(it.value, it.key) }.toMap()

    val targetMap = HashMap<Int,Int>()
    targetMap.put(genoidMap["B73_single"] ?: 0, genoidMap["B73_Assembly"] ?: 0)
    targetMap.put(genoidMap["B73_paired"] ?: 0, genoidMap["B73_Assembly"] ?: 0)
    targetMap.put(genoidMap["CML247_single"] ?: 0, genoidMap["CML247_Assembly"] ?: 0)
    targetMap.put(genoidMap["CML247_paired"] ?: 0, genoidMap["CML247_Assembly"] ?: 0)
    targetMap.put(genoidMap["Oh43_single"] ?: 0, genoidMap["Oh43_Assembly"] ?: 0)
    targetMap.put(genoidMap["Oh43_paired"] ?: 0, genoidMap["Oh43_Assembly"] ?: 0)

    //method hash
    val methodMaps = HashMap<Int, Map<String,String>>()
    val sqlMethod = "SELECT method_id, name, description FROM methods WHERE name like 'Read%'"
    val resultMethod = conn.createStatement().executeQuery(sqlMethod)
    while (resultMethod.next()) {
        val detailMap = DBLoadingUtils.parseMethodJsonParamsToString(resultMethod.getString(3))
        val deqoutedMap = detailMap.entries.map { Pair(it.key.replace("\"",""), it.value.replace("\"","")) }.toMap()
        methodMaps.put(resultMethod.getInt(1), deqoutedMap)
    }
    resultMethod.close()

    //get hash set of hapids for the refRange method
    val sqlHapRef = "SELECT haplotypes_id, ref_range_ref_range_method.method_id FROM haplotypes, ref_range_ref_range_method " +
            "WHERE haplotypes.ref_range_id=ref_range_ref_range_method.ref_range_id " +
            "AND ref_range_ref_range_method.method_id=$refRangeMethodId"
    println("Executing SQL: $sqlHapRef")
    val hapidMethodSet = HashSet<Int>()
    val resultHapRef = conn.createStatement().executeQuery(sqlHapRef)
    while(resultHapRef.next()) {
        hapidMethodSet.add(resultHapRef.getInt(1))
    }

    //get readMappings
    val summaryList = ArrayList<ReadMappingSummary>()
    val sqlReads = "SELECT read_mapping_id, genoid, method_id, file_group_name, mapping_data FROM read_mapping"
    val resultReads = conn.createStatement().executeQuery(sqlReads)
    var numberOfReadsProcessed = 0
    while (resultReads.next()) {
        //decompress the read mapping to a Map<List<Int>,Int>
        val detailMap = methodMaps[resultReads.getInt("method_id")]
        val pangenomeIndex = if (detailMap != null) detailMap["minimap2IndexFile"] else null
        val haplotypeMethod = if (pangenomeIndex == null) "NA" else indexFileToHaplotypeMethod(pangenomeIndex)
        val readGenoid = resultReads.getInt("genoid")
        val targetGenoid = targetMap[readGenoid]
        check(targetGenoid != null) {"target genoid is null for genoid = ${resultReads.getInt("genoid")}"}
        val targetHapidSet = getHapidSetForTarget(conn, targetGenoid, haplotypeMethod)

        //the next line filters the list of HapSetCounts to include only those with hapids in the specified haplotype method:ref range method
        val hits = decodeHapIdMapping(resultReads.getBytes("mapping_data"))
            .filter { entry -> entry.key.any { hapidMethodSet.contains(it) } }
        val numberOfReads = hits.values.sum()
        val numberOfCorrectReads = hits.entries.filter { entry ->
            val hapids = entry.key
            //does one of the hapids match the target?
            hapids.any { targetHapidSet.contains(it) }

        }.map { it.value }.sum()
        val maxErr = detailMap?.get("maxRefRangeErr")?.toDouble() ?: -1.0
        val maxSec = detailMap?.get("maxSecondary")?.toInt() ?: -1
        val fParam = detailMap?.get("fParameter") ?: "NA"
        val lineName = lineNameMap[readGenoid] ?: "NA"
        val readMethod = detailMap?.get("methodName") ?: "NA"
        val paired = lineName.endsWith("paired")
        summaryList.add(ReadMappingSummary(lineName, readMethod, haplotypeMethod, maxErr, maxSec, fParam, paired, numberOfReads, numberOfCorrectReads))
        numberOfReadsProcessed++
        println("$numberOfReadsProcessed rows from read_mapping processed")
    }

    return summaryList
}

private fun summarizeMappingResultsByMethod1(refRangeMethodId : Int) : List<ReadMappingSummary> {
    //for each read mapping calculate number of reads mapped and number of reads mapped correctly
    val conn = DBLoadingUtils.connection(false)

    //get hash set of hapids for the refRange method
    val sqlHapRef = "SELECT haplotypes_id, ref_range_ref_range_method.method_id FROM haplotypes, ref_range_ref_range_method " +
            "WHERE haplotypes.ref_range_id=ref_range_ref_range_method.ref_range_id " +
            "AND ref_range_ref_range_method.method_id=$refRangeMethodId"
    println("Executing SQL: $sqlHapRef")
    val hapidMethodSet = HashSet<Int>()
    val resultHapRef = conn.createStatement().executeQuery(sqlHapRef)
    while(resultHapRef.next()) {
        hapidMethodSet.add(resultHapRef.getInt(1))
    }

    //get contents of the read mapping table
    val sqlMappings = "SELECT read_mapping_id, genoid, methods.method_id, name, file_group_name FROM read_mapping, methods " +
            "WHERE read_mapping.method_id=methods.method_id"
    val mappingInfo = ArrayList<ReadMappingInfo>()
    val resultMappings = conn.createStatement().executeQuery(sqlMappings)
    println("executing query: $sqlMappings")
    while (resultMappings.next()) {
        mappingInfo.add(ReadMappingInfo(resultMappings.getInt(1), resultMappings.getInt(2),
            resultMappings.getInt(3), resultMappings.getString(4), resultMappings.getString(5)))
    }
    resultMappings.close()

    //which genoids have mummer4 haplotypes? Get the genoid and line_name. Store those as a list of genoids and a genoid -> name map.
    val sqlGenoid = "SELECT DISTINCT genotypes.genoid, line_name FROM genotypes, gamete_haplotypes, gametes " +
            "WHERE gamete_haplotypes.gameteid=gametes.gameteid AND gametes.genoid=genotypes.genoid " +
            "AND gamete_haplotypes.gamete_grp_id in (SELECT DISTINCT gamete_grp_id " +
            "FROM haplotypes, methods WHERE haplotypes.method_id = methods.method_id AND methods.name='${nonConsensusHaplotypeMethod}')"
    val genoidList = ArrayList<Int>()
    val genoidNameMap = HashMap<Int, String>()
    val resultGenoid = conn.createStatement().executeQuery(sqlGenoid)
    while(resultGenoid.next()) {
        genoidList.add(resultGenoid.getInt(1))
        genoidNameMap.put(resultGenoid.getInt(1), resultGenoid.getString(2))
    }
    resultGenoid.close()

    //is the read mapping for one of the taxa that has non-consensus haplotypes (mummer4)?
    val mummer4Mappings = mappingInfo.filter { genoidList.contains(it.genoId) }
    val summaryList = ArrayList<ReadMappingSummary>()

    //after filtering the set of reads mappings to those with mummer4 haplotypes, summarize the results
    mummer4Mappings.forEach {
        //get the read mapping
        val sqlMapping = "SELECT mapping_data FROM read_mapping WHERE read_mapping_id = ${it.readId}"
        val resultMapping = conn.createStatement().executeQuery(sqlMapping)

        //count total and correctly mapped reads
        if (resultMapping.next()) {
            //decompress the read mapping to a Map<List<Int>,Int>
            val hits = decodeHapIdMapping(resultMapping.getBytes(1))
            val detailMap = getMethodDetails(conn, it.methodId)
            val pangenomeIndex = detailMap?.get("minimap2IndexFile")
            val haplotypeMethod = if (pangenomeIndex == null) "NA" else indexFileToHaplotypeMethod(pangenomeIndex)
            val hapidSet = getHapidSetForTarget(conn, it.genoId, haplotypeMethod)

            //filter the hits on reference range method
            val method_hits = hits.entries.filter { entry ->
                val hapids = entry.key
                hapidMethodSet.contains(hapids[0])
            }.toList()


            val numberOfReads = method_hits.map { entry -> entry.value }.sum()

            val numberOfCorrectReads = method_hits.filter { entry ->
                entry.key.any { hapidSet.contains(it) }
            }.map { entry -> entry.value }.sum()

            val maxErr = detailMap?.get("maxRefRangeErr")?.toDouble() ?: -1.0
            val maxSec = detailMap?.get("maxSecondary")?.toInt() ?: -1
            val fParam = detailMap?.get("fParameter") ?: "NA"
            val lineName = genoidNameMap[it.genoId] ?: "NA"
            val readMethod = it.methodName
            val paired = it.fileGroup.contains("paired")
            summaryList.add(ReadMappingSummary(lineName, readMethod, haplotypeMethod, maxErr, maxSec, fParam, paired, numberOfReads, numberOfCorrectReads))
        } else myLogger.error("no mapping for read_mapping_id = ${it.readId}")
    }

    conn.close()
    return summaryList
}

private fun getMethodDetails(conn : Connection, methodId: Int) : Map<String, String>? {
    val sql = "SELECT name, description FROM methods WHERE method_id = $methodId"
    val result = conn.createStatement().executeQuery(sql)
    if (result.next()) {
        val detailMap = DBLoadingUtils.parseMethodJsonParamsToString(result.getString(2))
        val deqoutedMap = detailMap.entries.map { Pair(it.key.replace("\"",""), it.value.replace("\"","")) }.toMap()
        return deqoutedMap
    }
    result.close()
    return null;
}

private fun indexFileToHaplotypeMethod (filename: String) : String {
    val startIndex = filename.indexOf("pangenome_") + 10
    val endIndex = filename.indexOf(".mmi")
    return filename.substring(startIndex, endIndex)
}

private fun getSortedHapidsForTarget(conn : Connection, genoid : Int, haplotypeMethod : String) : List<Int> {
    val sql = "SELECT DISTINCT haplotypes_id FROM haplotypes, methods, gamete_haplotypes, gametes WHERE methods.name = '$haplotypeMethod' " +
            "AND haplotypes.method_id = methods.method_id AND gametes.genoid = $genoid " +
            "AND gamete_haplotypes.gameteid=gametes.gameteid AND gamete_haplotypes.gamete_grp_id=haplotypes.gamete_grp_id " +
            "ORDER BY haplotypes_id"
    val result = conn.createStatement().executeQuery(sql)
    val hapidList = ArrayList<Int>()
    while (result.next()) {
        hapidList.add(result.getInt(1))
    }
    result.close()
    return hapidList
}

private fun getHapidSetForTarget(conn : Connection, genoid : Int, haplotypeMethod : String) : Set<Int> {
    val sql = "SELECT DISTINCT haplotypes_id FROM haplotypes, methods, gamete_haplotypes, gametes WHERE methods.name = '$haplotypeMethod' " +
            "AND haplotypes.method_id = methods.method_id AND gametes.genoid = $genoid " +
            "AND gamete_haplotypes.gameteid=gametes.gameteid AND gamete_haplotypes.gamete_grp_id=haplotypes.gamete_grp_id " +
            "ORDER BY haplotypes_id"
    val result = conn.createStatement().executeQuery(sql)
    val hapidSet = HashSet<Int>()
    while (result.next()) {
        hapidSet.add(result.getInt(1))
    }
    result.close()
    return hapidSet
}

private fun getHapidSetForMethod(conn : Connection, haplotypeMethod : String, refRangeMethodId : Int) : Set<Int> {
    val sqlHapid = "SELECT haplotypes_id " +
            "FROM haplotypes, methods, ref_range_ref_range_method " +
            "WHERE ref_range_ref_range_method.method_id = $refRangeMethodId " +
            "AND ref_range_ref_range_method.ref_range_id = haplotypes.ref_range_id " +
            "AND haplotypes.method_id = methods.method_id " +
            "AND methods.name = '$haplotypeMethod'"
    val resultHapid = conn.createStatement().executeQuery(sqlHapid)
    val hapidSet = HashSet<Int>()
    while (resultHapid.next()) {
        hapidSet.add(resultHapid.getInt(1))
    }
    resultHapid.close()
    return hapidSet
}

data class FindPathParameters(val readMethod: String, val pathMethod: String, val keyFile: String, val minTaxa : Int, val minReads : Int, val minTransitionProb : Double,
                              val probCorrect : Double, val splitProb : Double, val algorithmType : BestHaplotypePathPlugin.ALGORITHM_TYPE)

fun findPaths(parameters : FindPathParameters, graphDS : DataSet) {
    myLogger.info("Finding paths for keyfile ${parameters.keyFile}")
    BestHaplotypePathPlugin(null, false)
        .readMethodName(parameters.readMethod)
        .pathMethodName(parameters.pathMethod)
        .keyFile(parameters.keyFile)
        .minTaxaPerRange(parameters.minTaxa)
        .minReads(parameters.minReads)
        .minTransitionProb(parameters.minTransitionProb)
        .probReadMappedCorrectly(parameters.probCorrect)
        .splitTransitionProb(parameters.splitProb)
        .algorithmType(parameters.algorithmType)
        .performFunction(graphDS)
}

private fun findPathsScenario1() {

    val pathMethodMap = mapOf<String, String>(
        "PathMethod1" to "keyfile_CONSENSUS1_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod2" to "keyfile_CONSENSUS2_0.25_30_f10000,11000_pathKeyFile.txt",
        "PathMethod3" to "keyfile_CONSENSUS2_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod4" to "keyfile_CONSENSUS2_0.25_30_f15000,16000_pathKeyFile.txt",
        "PathMethod5" to "keyfile_CONSENSUS2_0.25_30_f20000,21000_pathKeyFile.txt",
        "PathMethod6" to "keyfile_CONSENSUS2_0.25_30_f25000,26000_pathKeyFile.txt",
        "PathMethod7" to "keyfile_CONSENSUS2_0.25_30_f5000,6000_pathKeyFile.txt",
        "PathMethod8" to "keyfile_CONSENSUS3_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod9" to "keyfile_CONSENSUS4_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod10" to "keyfile_CONSENSUS5_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod11" to "keyfile_CONSENSUS6_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod12" to "keyfile_CONSENSUS7_0.25_30_f10000,11000_pathKeyFile.txt",
        "PathMethod13" to "keyfile_CONSENSUS7_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod14" to "keyfile_CONSENSUS7_0.25_30_f15000,16000_pathKeyFile.txt",
        "PathMethod15" to "keyfile_CONSENSUS7_0.25_30_f20000,21000_pathKeyFile.txt",
        "PathMethod16" to "keyfile_CONSENSUS7_0.25_30_f25000,26000_pathKeyFile.txt",
        "PathMethod17" to "keyfile_CONSENSUS7_0.25_30_f5000,6000_pathKeyFile.txt",
        "PathMethod18" to "keyfile_CONSENSUS8_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod19" to "keyfile_CONSENSUS9_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod20" to "keyfile_CONSENSUS10_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod21" to "keyfile_mummer4_0.05_30_f1000,5000_pathKeyFile.txt",
        "PathMethod22" to "keyfile_mummer4_0.1_30_f1000,5000_pathKeyFile.txt",
        "PathMethod23" to "keyfile_mummer4_0.25_30_f10000,11000_pathKeyFile.txt",
        "PathMethod24" to "keyfile_mummer4_0.25_30_f1000,5000_pathKeyFile.txt",
        "PathMethod25" to "keyfile_mummer4_0.25_30_f15000,16000_pathKeyFile.txt",
        "PathMethod26" to "keyfile_mummer4_0.25_30_f20000,21000_pathKeyFile.txt",
        "PathMethod27" to "keyfile_mummer4_0.25_30_f25000,26000_pathKeyFile.txt",
        "PathMethod28" to "keyfile_mummer4_0.25_30_f5000,6000_pathKeyFile.txt",
        "PathMethod29" to "keyfile_mummer4_0.5_30_f1000,5000_pathKeyFile.txt" )


    val dbConn = DBLoadingUtils.connection(false)
    pathMethodMap.entries.forEach {
        //what read mapping method was used?

        val haplotypeMethod = it.value.substring(8, it.value.indexOf("_", 8))
        println("haplotype method for ${it.key} is $haplotypeMethod")
        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .methods(haplotypeMethod)
            .includeVariantContexts(false)
            .configFile(configFile)
            .performFunction(null)

        val pathTokeyFile = "${fastqDir}/${it.value}"
        val keyFileReader = Files.newBufferedReader(Paths.get(pathTokeyFile))
        keyFileReader.readLine()
        val readId = keyFileReader.readLine().split('\t')[1].toInt()
        val resultSet = dbConn.createStatement().executeQuery("select name from methods, " +
                "read_mapping where methods.method_id=read_mapping.method_id and read_mapping_id = $readId")
        resultSet.next()
        val readMethod = resultSet.getString(1)
        resultSet.close()

        val param = FindPathParameters(readMethod = readMethod,
            pathMethod = it.key,
            keyFile = pathTokeyFile,
            minTaxa = 10,
            minReads = 0,
            minTransitionProb = .000001,
            probCorrect = 0.99,
            splitProb = 0.99,
            algorithmType = BestHaplotypePathPlugin.ALGORITHM_TYPE.classic)

        findPaths(param, graphDS)

    }
    dbConn.close()
}

private fun findPathsScenario2() {
    //paths for fParam variants

    val pathMethodMap = mapOf<String, String>(

    )
    val dbConn = DBLoadingUtils.connection(false)
    pathMethodMap.entries.forEach {
        //what read mapping method was used?

        val haplotypeMethod = it.value.substring(8, it.value.indexOf("_", 8))
        println("haplotype method for ${it.key} is $haplotypeMethod")
        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .methods(haplotypeMethod)
            .includeVariantContexts(false)
            .configFile(configFile)
            .performFunction(null)

        val pathTokeyFile = "${fastqDir}/${it.value}"
        val keyFileReader = Files.newBufferedReader(Paths.get(pathTokeyFile))
        keyFileReader.readLine()
        val readId = keyFileReader.readLine().split('\t')[1].toInt()
        val resultSet = dbConn.createStatement().executeQuery("select name from methods, " +
                "read_mapping where methods.method_id=read_mapping.method_id and read_mapping_id = $readId")
        resultSet.next()
        val readMethod = resultSet.getString(1)
        resultSet.close()

        val param = FindPathParameters(readMethod = readMethod,
            pathMethod = it.key,
            keyFile = pathTokeyFile,
            minTaxa = 10,
            minReads = 0,
            minTransitionProb = .000001,
            probCorrect = 0.99,
            splitProb = 0.99,
            algorithmType = BestHaplotypePathPlugin.ALGORITHM_TYPE.classic)

        findPaths(param, graphDS)

    }
    dbConn.close()
}

fun haploidPathSummary(pathMethod: String, summaryFile: String) {
    //number of haplotypes called, number called correctly
    //where genoid of path matches genoid for haplotypes
    //need to know haplotype method used to call the path

    val hapMethodid = haplotypeMethodIdFromPathMethodName(pathMethod)
    val dbConn = DBLoadingUtils.connection(false)

    //get a resultSet with paths
    val pathQuery = "SELECT path_id, paths.genoid, line_name, paths_data FROM paths, methods, genotypes " +
            "WHERE paths.method_id=methods.method_id AND methods.name = '${pathMethod}' AND paths.genoid=genotypes.genoid"
    println("executing query: $pathQuery")
    val pathRS = dbConn.createStatement().executeQuery(pathQuery)

    PrintWriter(summaryFile).use {pw ->
        pw.println("genoid\tname\tnCalled\tnCorrect\tprCorrect")
        while (pathRS.next()) {
            val pathGenotypeName = pathRS.getString(3)
            val hapName = pathGenotypeName.substring(0, pathGenotypeName.indexOf("_")) + "_Assembly"
            //for each path that has a genoid in haplotypes, report number of haplotypes called, number called correctly, proportion called correctly
            val hapidQuery = "SELECT haplotypes_id " +
                    "FROM haplotypes, gamete_haplotypes, gametes, genotypes " +
                    "WHERE haplotypes.method_id=${hapMethodid} " +
                    "AND haplotypes.gamete_grp_id=gamete_haplotypes.gamete_grp_id " +
                    "AND gamete_haplotypes.gameteid=gametes.gameteid " +
                    "AND gametes.genoid=genotypes.genoid AND genotypes.line_name = '$hapName'"
            println("executing hapidQuery: ${hapidQuery}")
            val targetHapids = HashSet<Int>()
            val hapidRS = dbConn.createStatement().executeQuery(hapidQuery)
            while (hapidRS.next()) targetHapids.add(hapidRS.getInt(1))
            hapidRS.close()

            val hapidStr = targetHapids.stream().limit(5).map { it.toString() }.collect(Collectors.joining(","))
            println("The target hapids contain ${targetHapids.size} ids. Some of those are ${hapidStr}")

            val pathList = DBLoadingUtils.decodePathsArray(pathRS.getBytes(4))
            val nHaps = pathList.size
            val pathHapidSample = Arrays.stream(pathList).limit(5).boxed().map{ p -> p.toString() }.collect(Collectors.joining(","))
            println("Sample of hapids from the path = ${pathHapidSample}")
            val nCorrect = pathList.filter { targetHapids.contains(it) }.count()
            val prCorrect = if (nHaps > 0) nCorrect.toDouble() / nHaps.toDouble() else 0.0
            pw.println("${pathRS.getInt(2)}\t${pathRS.getString(3)}\t${nHaps}\t${nCorrect}\t${prCorrect}")
        }

    }

    pathRS.close()
    dbConn.close()
}

private fun haplotypeMethodIdFromPathMethodName(pathMethodName: String) : Int {
    //SELECT DISTINCT read_mapping.method_id FROM paths, methods, read_mapping_paths, read_mapping  WHERE methods.name = "PathMethod1" AND methods.method_id=paths.method_id AND paths.path_id=read_mapping_paths.path_id AND read_mapping_paths.read_mapping_id=read_mapping.read_mapping_id
    val dbConn = DBLoadingUtils.connection(false)
    val methodidQuery = "SELECT DISTINCT read_mapping.method_id " +
            "FROM paths, methods, read_mapping_paths, read_mapping  " +
            "WHERE methods.name = '${pathMethodName}' " +
            "AND methods.method_id=paths.method_id " +
            "AND paths.path_id=read_mapping_paths.path_id " +
            "AND read_mapping_paths.read_mapping_id=read_mapping.read_mapping_id"

    val methodidList = ArrayList<Int>()
    val methodidRS = dbConn.createStatement().executeQuery(methodidQuery)
    while (methodidRS.next()) {
        methodidList.add(methodidRS.getInt(1))
    }
    methodidRS.close()

    if (methodidList.size == 0) throw IllegalArgumentException("No read_mapping_id for path method name ${pathMethodName}.")
    if (methodidList.size > 1) throw IllegalArgumentException("More than one read_mapping_id for path method name ${pathMethodName}.")

    //get the haplotype name from the method description
    val descQuery = "SELECT description FROM methods WHERE method_id = ${methodidList[0]}"
    val descRS = dbConn.createStatement().executeQuery(descQuery)
    descRS.next()
    val description = descRS.getString(1)
    descRS.close()

    val descMap = DBLoadingUtils.parseMethodJsonParamsToString(description)
    val indexPathname = descMap["minimap2IndexFile"]
    val indexFilename = File(indexPathname).nameWithoutExtension
    val hapMethodName = indexFilename.substring(10)

    if (hapMethodName.length == 0) throw IllegalArgumentException("hapMethodName has length = 0.")

    val hapMethodQuery = "SELECT method_id FROM methods WHERE name = '${hapMethodName}'"
    println("executing query: $hapMethodQuery")
    val hapMethodRS = dbConn.createStatement().executeQuery(hapMethodQuery)
    hapMethodRS.next()
    val hapMethodid = hapMethodRS.getInt(1)
    hapMethodRS.close()

    dbConn.close()
    myLogger.info("for path method ${pathMethodName} haplotype method is ${hapMethodName}")
    return hapMethodid

}

fun exportMethodDescriptions(filename:String) {
    val consensusQuery = "SELECT method_id, method_type, name, description FROM methods WHERE name like 'CON%'"
    exportDescriptionsFromQuery("${filename}_consensus.txt", consensusQuery)
    val readMappingQuery = "SELECT method_id, method_type, name, description FROM methods WHERE name like 'Read%'"
    exportDescriptionsFromQuery("${filename}_readMapping.txt", readMappingQuery)
}

fun exportDescriptionsFromQuery(filename: String, methodQuery : String) {
    val dbConn = DBLoadingUtils.connection(false)
    val methodRS = dbConn.createStatement().executeQuery(methodQuery)

    PrintWriter(filename).use { pw ->
        var first = true
        val parmNames = ArrayList<String>()
        while (methodRS.next()) {
            val desc = DBLoadingUtils.parseMethodJsonParamsToString(methodRS.getString("description"))
            if (first) {
                first = false
                pw.print("method_id\t name ")
                desc.entries.forEach {
                    pw.print("\t ${it.key}")
                    parmNames.add(it.key)
                }
                pw.println()
            }
            pw.print("${methodRS.getInt("method_id")}\t${methodRS.getString("name")}")
            parmNames.map { desc[it] ?: "NA" }.forEach { pw.print("\t$it") }
            pw.println()
        }

    }
    dbConn.close()

}

fun pathKeyFileFromMappingId(mappingIdFile: String) {
    val baseName = mappingIdFile.substring(0, mappingIdFile.indexOf("_withMappingIds.txt"))
    val keyFileName = "${baseName}_pathKeyFile.txt"
    PrintWriter(keyFileName).use { pw ->
        pw.println("SampleName\tReadMappingIds\tLikelyParents")
        Files.newBufferedReader(Paths.get(mappingIdFile)).lines().skip(1).forEach {
            val lineValues = it.split('\t')
            val sampleName = "${lineValues[0].substring(0, lineValues[0].indexOf("_Assembly"))}_${lineValues[1]}"
            pw.println("${sampleName}\t${lineValues[5]}\t")
        }
    }
}

fun rewritePathKeyFilesInFastqDir() {
    File(fastqDir).list().filter { it.contains("_withMappingIds.txt") }.forEach { pathKeyFileFromMappingId("${fastqDir}/$it") }
}

fun pathMethodSummary(outfile : String) {
    //what about genic vs inter-genic?
    //retrieve and parse path method descriptions
    //for each path
    //number of haplotypes imputed
    //number of haplotypes imputed as not missing but missing in target
    //number of haplotypes imputed as missing but present in target
    //for haplotypes imputed as not missing and present in target, count number target SNPs not imputed correctly
    //and number of imputed SNPs not in target
    //average by path method
    //report along with path method descriptors

    val dbConn = DBLoadingUtils.connection(false)

    //need a hapid -> referenceRange map for subsequent analysis (to match query to target haplotype by reference range)
    /*CREATE TABLE haplotypes (
    haplotypes_id INTEGER PRIMARY KEY,
    gamete_grp_id INTEGER,
    ref_range_id INTEGER NOT NULL,
    sequence BLOB,
    seq_len INTEGER,
    seq_hash text,
    method_id INTEGER,
    variant_list BLOB,
    UNIQUE (gamete_grp_id, ref_range_id, method_id),
    FOREIGN KEY (ref_range_id) REFERENCES reference_ranges(ref_range_id),
    FOREIGN KEY (method_id) REFERENCES methods(method_id)
);
CREATE TABLE reference_ranges (
    ref_range_id INTEGER PRIMARY KEY,
    chrom	TEXT,
    range_start INTEGER NOT NULL,
    range_end INTEGER NOT NULL,
    UNIQUE (chrom,range_start)
);
*/
    val hapidRefrangeMap = HashMap<Int,Int>()
    val queryHapidMap = "SELECT haplotypes_id, ref_range_id FROM haplotypes"
    println("Executing query: ${queryHapidMap}")
    val resultHapidMap = dbConn.createStatement().executeQuery(queryHapidMap)
    while (resultHapidMap.next()) {
        hapidRefrangeMap.put(resultHapidMap.getInt(1), resultHapidMap.getInt(2))
    }
    resultHapidMap.close()

    val methods = methodDescriptors(dbConn)

    PrintWriter(outfile).use {pw ->
        pw.println("methodId\tpathMethod\treadMethod\ttaxon\tnHaps\tnCorrect\tpSNP\tnHapsNotTarget")
        methods.filter { it.description.containsKey("pathMethod") }.forEach {desc ->

            //what is the correct haplotype method id for this path?
            val keyfileName = desc.description["keyFile"]
            val methodName = keyfileName?.substringAfter("keyfile_")?.substringBefore("_") ?: ""

            //target haplotypes
            val targetHaplotypes = targetHapids(dbConn, methodName)
            targetHaplotypes.entries.forEach { ent ->
                println("Targets: ${ent.key} has ${ent.value.size} hapids for $methodName")
            }

            val queryPathsForMethod = "SELECT path_id, paths.genoid, line_name, paths_data " +
                    "FROM paths, genotypes WHERE method_id = ${desc.id} AND paths.genoid=genotypes.genoid"
            val resultPathsForMethod = dbConn.createStatement().executeQuery(queryPathsForMethod)

            //counters
            println("Executing : $queryPathsForMethod")
            while (resultPathsForMethod.next()) {
                val hapids = DBLoadingUtils.decodePathsForMultipleLists(resultPathsForMethod.getBytes(4))[0]
                val numberOfHaplotypes = hapids.size
                val taxonName = resultPathsForMethod.getString(3)
                val targetIdSet = targetHaplotypes["${taxonName.substringBefore('_')}_Assembly"]
                check(targetIdSet != null) {"targetIdSet null for path_id = ${resultPathsForMethod.getInt(1)}, genoid = ${resultPathsForMethod.getInt(2)}, line_name = ${resultPathsForMethod.getString(3)}"}
                val hapidsNotInTargetSet = hapids.filter { !targetIdSet.contains(it) }
                val numberOfHaplotypesInTarget = hapids.size - hapidsNotInTargetSet.size

                println("For $taxonName: numberOfHaplotypesInTarget = $numberOfHaplotypesInTarget, number not in target = ${hapidsNotInTargetSet.size}")
                //calculate number of incorrect variants
                //TODO this only counts positions in haplotypes for which query and target do not match
                //   It should the position count should prabably include counts from haplotypes that do match.
                var numberOfHaplotypesCalledButNotInTarget = 0
                var numberOfSNP = 0
                var numberOfPositions = 0

                //counts all haplotypes (preferred, comment block counting subset of haplotypes)
                val refRange2TargetHapid = targetIdSet.associateBy { hapidRefrangeMap[it] }
                for (hapid in hapids) {
                    //need the refrange of this hapid, then the target hapid in the same refRange
                    val pathRefRange = hapidRefrangeMap[hapid]
                    val targetHapid = refRange2TargetHapid[pathRefRange]
                    if (targetHapid == null) numberOfHaplotypesCalledButNotInTarget++
                    else {
                        val comparisonCountPair = compareVariants(dbConn, hapid, targetHapid)
                        numberOfSNP += comparisonCountPair.first
                        numberOfPositions += comparisonCountPair.second
                    }
                }

                //only counts sites in haplotypes that are different
//                if (hapidsNotInTargetSet.size > 0) {
//                    val refRange2TargetHapid = targetIdSet.associateBy { hapidRefrangeMap[it] }
//                    for (hapid in hapidsNotInTargetSet) {
//                        //need the refrange of this hapid, then the target hapid in the same refRange
//                        val pathRefRange = hapidRefrangeMap[hapid]
//                        val targetHapid = refRange2TargetHapid[pathRefRange]
//                        if (targetHapid == null) numberOfHaplotypesCalledButNotInTarget++
//                        else {
//                            val comparisonCountPair = compareVariants(dbConn, hapid, targetHapid)
//                            numberOfSNP += comparisonCountPair.first
//                            numberOfPositions += comparisonCountPair.second
//                        }
//                    }
//                }

                //"methodId\tpathMethod\treadMethod\ttaxon\tnHaps\tnCorrect\tpSNP\tnHapRefrangesNotInTarget"
                pw.println("${desc.id}\t${desc.name}\t${desc.description["readMethod"]}\t${taxonName}" +
                        "\t${numberOfHaplotypes}\t${numberOfHaplotypesInTarget}\t${numberOfSNP.toDouble()/numberOfPositions.toDouble()}\t${numberOfHaplotypesCalledButNotInTarget}")
            }
            resultPathsForMethod.close()
        }
    }


    dbConn.close()
}

data class MethodDescriptor(val id: Int, val name: String, val description: Map<String,String>)

fun methodDescriptors(dbConn : Connection) : List<MethodDescriptor> {
    val queryMethods = "select method_id, name, description from methods"
    val resultMethods = dbConn.createStatement().executeQuery(queryMethods)
    val methodInfo = mutableListOf<MethodDescriptor>()
    while (resultMethods.next()) {
        methodInfo.add(MethodDescriptor(resultMethods.getInt(1),
            resultMethods.getString(2),
            DBLoadingUtils.parseMethodJsonParamsToString(resultMethods.getString(3))))
    }
    resultMethods.close()
    return methodInfo
}

/**
 * @return a map of line_name to list of hapids for B73_Assembly, OH43_Assembly, and CML247_Assembly
 */
fun targetHapids(dbConn: Connection, methodName: String) : Map<String, HashSet<Int>> {
    val query = "SELECT line_name, haplotypes_id " +
            "FROM haplotypes, gamete_haplotypes, gametes, genotypes, methods " +
            "WHERE haplotypes.gamete_grp_id=gamete_haplotypes.gamete_grp_id " +
            "AND gamete_haplotypes.gameteid=gametes.gameteid " +
            "AND gametes.genoid=genotypes.genoid " +
            "AND haplotypes.method_id=methods.method_id AND methods.name='$methodName' " +
            "AND line_name in ('B73_Assembly','Oh43_Assembly','CML247_Assembly')"
    println("Getting Target Hapids\nQuery: $query")

    val targetList = ArrayList<Pair<String,Int>>()
    val result = dbConn.createStatement().executeQuery(query)
    while (result.next()) {
        targetList.add(Pair(result.getString(1), result.getInt(2)))
    }

    result.close()

    //the group by produces a map of line_name -> List of hapids
    //map makes the list of hapids into a hash map for performance of contains
    return targetList.groupBy({it.first},{it.second})
        .entries.map { Pair(it.key, HashSet<Int>(it.value)) }.toMap()

}

/**
 * @return  The number of SNPs between hapid1 and hapid2. That is, sites at which one haplotype carries the alt allele of a SNP
 * and the other haplotype carries a ref allele
 */
fun compareVariants(dbConn: Connection, hapid1: Int, hapid2: Int) : Pair<Int,Int> {
    if (hapid1 == hapid2) {
        //count the number of non-N's in the sequence and return  Pair(0, non-N count)
        val query = "SELECT sequence, haplotypes_id FROM haplotypes WHERE haplotypes_id = $hapid1"

        return dbConn.createStatement().use {stmnt ->
            stmnt.executeQuery(query).use { result ->
                result.next()
                if (result.isAfterLast) {
                    println("Queried hapids. No variant data for hapids $hapid1 and $hapid2")
                    Pair(0,0)
                }
                val seq = GZipCompression.decompress(result.getBytes(1))
                val nonNSites = seq.filter { it != 'N' && it !='n' }.count()
                Pair(0,nonNSites)
            }
        }

    }

    val query = "SELECT variant_list, haplotypes_id FROM haplotypes WHERE haplotypes_id in ($hapid1, $hapid2)"
    val result = dbConn.createStatement().executeQuery(query)
    result.next()

    if (result.isAfterLast) {
        println("Queried hapids. No variant data for hapids $hapid1 and $hapid2")
        return Pair(0,0)
    }
    val variant1 = VariantsProcessingUtils.decodeByteArrayToVariantLongList(result.getBytes(1))
        .map { VariantUtils.decodeLongVariant(it) }
    val returnedHapid = result.getInt(2)
    result.next()
    if (result.isAfterLast) {
        println("Queried hapids $hapid1 and $hapid2. Variant data for $returnedHapid only.")
        return Pair(0,0)
    }
    val variant2 = VariantsProcessingUtils.decodeByteArrayToVariantLongList(result.getBytes(1))
        .map { VariantUtils.decodeLongVariant(it) }
    result.close()

    //The contents of the int arrays in the variant1 and lists can be
    //int[] is 1, variant id, refDepth, altDepth.
    //int[] is -1, block length, read depth, the block chromosomal position.
    //get information for non-refblock variants from db

    //variantIdSet is the set of all variants created in order
    val variantIdSet = variant1.filter { it[0] == 1 }.map { it[1] }.toMutableSet()
    val variantMap = HashMap<Int, VariantValues>()
    variant2.filter { it[0] == 1 }.forEach { variantIdSet.add(it[1]) }
    val variantString = variantIdSet.map{it.toString()}.joinToString(",")
    val queryVariants = "SELECT variant_id, chrom, position, a.len AS ref_len, b.len AS alt_len " +
            "FROM variants, alleles a, alleles b " +
            "WHERE a.allele_id=variants.ref_allele_id AND b.allele_id=variants.alt_allele_id AND variant_id IN ($variantString)"
    val resultVariants = dbConn.createStatement().executeQuery(queryVariants)
    while (resultVariants.next()) {
        variantMap.put(resultVariants.getInt(1),
            VariantValues(resultVariants.getString(2),
                resultVariants.getInt(3),
                resultVariants.getInt(4),
                resultVariants.getInt(5)))
    }
    resultVariants.close()
    val variantRangeMap1 = makeRangeMap(variantMap, variant1)
    val variantRangeMap2 = makeRangeMap(variantMap, variant2)
    val counts = compareRangeMaps(variantRangeMap1, variantRangeMap2, variantMap)
    println("For $hapid1, $hapid2 : number of snps = ${counts.first}, number of sites = ${counts.second}")
    return counts
}

/**
 * compares variants in two range maps. Each position that is either a reference call or a SNP for both maps is a volid position.
 * If one or the other is an alt call and a SNP, the position counts as a SNP.
 */
fun compareRangeMaps(variantRangeMap1: RangeMap<Int, IntArray>, variantRangeMap2: RangeMap<Int, IntArray>, variantMap : Map<Int, VariantValues>) : Pair<Int,Int> {
    val span1 = variantRangeMap1.span()
    val span2 = variantRangeMap2.span()
    var currentPos = Math.min(span1.lowerEndpoint(), span2.lowerEndpoint())
    val endPos = Math.max(span1.upperEndpoint(), span2.upperEndpoint())
    var numberPos = 0
    var numberSnps = 0
    println("currentPos = $currentPos, endPos = $endPos")
    var prevPos = currentPos
    while (currentPos <= endPos) {
        val varArray1 = variantRangeMap1[currentPos]
        val varArray2 = variantRangeMap2[currentPos]

        //do not count if either is null
        if (varArray1 == null || varArray2 == null) {
            currentPos++
            continue
        }

        if (varArray1[0] == -1) {
            //first variant is refblock, end = block length + position - 1
            val end1 = varArray1[1] + varArray1[3] - 1
            if (varArray2[0] == -1) {
                //second variant is refblock
                val end2 = varArray2[1] + varArray2[3] - 1
                val blockEnd = Math.min(end1, end2)
                //since both variants are refblocks skip to the first end, count sites and add to number of positions
                numberPos += Math.max(blockEnd - currentPos + 1, 0)
                currentPos = Math.max(blockEnd + 1, currentPos + 1)
            } else {
                if (isRef(varArray2)) {
                    //second variant is not in a ref block but has been called ref allele
                    //so advance only one position and add 1 to position count
                    numberPos++
                    currentPos++
                } else if (isAlt(varArray2)) {
                    //is variant2 a snp (not an indel), add 1 to positions and snps
                    if (!isIndel(varArray2, variantMap)) {
                        numberPos++
                        currentPos++
                        numberSnps++
                    } else {
                        //variant2 is an indel, skip to next position but do not increment position count
                        currentPos++
                    }
                } else currentPos++
            }
        } else {
            if (varArray2[0] == -1) {
                if (isRef(varArray1)) {
                    //first variant is not in a ref block but has been called ref allele
                    //so advance only one position and add 1 to position count
                    numberPos++
                    currentPos++
                } else if (isAlt(varArray1)) {
                    //is variant1 a snp (not an indel), add 1 to positions and snps
                    if (!isIndel(varArray1, variantMap)) {
                        numberPos++
                        currentPos++
                        numberSnps++
                    } else {
                        //variant1 is an indel, skip to next position but do not increment position count
                        currentPos++
                    }
                } else currentPos++
            } else {

                if (varArray1[1] != varArray2[1]) {
                    //neither variant is in refblock, if variantids are different, advance position but do not count
                    currentPos++
                } else {
                    if (isIndel(varArray1, variantMap)) {
                        //both variant ids are the same. If the variant is not a snp, advance position but do not count
                        currentPos++
                    } else {
                        //it is possible for a call to be unknown so cannot assume that isAlt and !isRef are the same
                        //both  can be false
                        val isRef1 = isRef(varArray1)
                        val isRef2 = isRef(varArray2)
                        val isAlt1 = isAlt(varArray1)
                        val isAlt2 = isAlt(varArray2)

                        if ((isRef1 && isRef2) || (isAlt1 && isAlt2)) {
                            //the variant is a snp, if both are ref or alt increment position count and advance 1
                            numberPos++
                        } else if ((isRef1 && isAlt2) || (isAlt1 && isRef2)) {
                            //if one is ref and one is alt increment snp count also
                            numberPos++
                            numberSnps++
                        }
                        currentPos++
                    }
                }
            }
        }
        if (currentPos <= prevPos) {
            println("prevPos = $prevPos, currentPos = $currentPos, quitting")
            return Pair(numberSnps, numberPos)
        }
        prevPos = currentPos
    }
    return Pair(numberSnps, numberPos)
}

private fun isRef(varArray : IntArray) : Boolean {
    return if (varArray[0] == -1) true
    else {
        if (varArray[2] > varArray[3]) true else false
    }
}

private fun isAlt(varArray : IntArray) : Boolean {
    return if (varArray[0] == -1) false
    else {
        if (varArray[2] < varArray[3]) true else false
    }
}

private fun isIndel(varArray : IntArray, variantMap: Map<Int, VariantValues>) : Boolean {
    return if (varArray[0] == -1) false
    else {
        val variant = variantMap[varArray[1]]
        val sumLength = if (variant == null) 0 else variant.refLength + variant.altLength
        if (sumLength == 2) false else true
    }
}

fun makeRangeMap(variantMap: Map<Int, VariantValues>, variantList: List<IntArray>) : RangeMap<Int, IntArray> {
    val myRangeMap = TreeRangeMap.create<Int, IntArray>()
    for (variantIntArray in variantList) {
        if (variantIntArray[0] == -1) {
            myRangeMap.put(Range.closed(variantIntArray[3], variantIntArray[3] + variantIntArray[1] - 1), variantIntArray)
        } else {
            //variant
            val varValues = variantMap[variantIntArray[1]]
            check(varValues != null) {"no values for variant id ${variantIntArray[1]}"}
            myRangeMap.put(Range.closed(varValues.pos, varValues.pos + varValues.refLength - 1), variantIntArray)
        }
    }
    return myRangeMap
}

data class VariantValues(val chr: String, val pos: Int, val refLength: Int, val altLength: Int)

fun testCompareVariants() {
    //int[] is 1, variant id, refDepth, altDepth.
    //int[] is -1, block length, read depth, the block chromosomal position.

    //add some variants to a map
    val variantMap = mapOf(1 to VariantValues("chr1", 5, 1,1),
        2 to VariantValues("chr1", 10, 1,1),
        3 to VariantValues("chr1", 15, 1,1),
        4 to VariantValues("chr1", 20, 2,1),
        5 to VariantValues("chr1", 25, 1,2))

    //create 2 range maps
    val varList1 = listOf(intArrayOf(-1, 30, 10, 1))
    val varList2 = listOf(intArrayOf(-1, 4, 10, 1), intArrayOf(1,1,0,10),
        intArrayOf(-1, 4, 10, 6), intArrayOf(1,2,0,10),
        intArrayOf(-1, 4, 10, 11), intArrayOf(1,3,0,10),
        intArrayOf(-1, 4, 10, 16), intArrayOf(1,4,0,10),
        intArrayOf(-1, 3, 10, 22), intArrayOf(1,5,0,10), intArrayOf(-1, 5, 10, 26))

    //should be 3 snps, 27 positions
    var counts = compareRangeMaps(makeRangeMap(variantMap, varList1), makeRangeMap(variantMap, varList2), variantMap)
    println("Snp count: ${counts.first}, position count: ${counts.second}")

    val varList3 = listOf(intArrayOf(-1, 4, 10, 1), intArrayOf(1,1,0,10),
        intArrayOf(-1, 4, 10, 6), intArrayOf(1,2,0,10),
        intArrayOf(-1, 4, 10, 11), intArrayOf(1,3,0,10),
        intArrayOf(-1,15, 10, 16 ))
    val varList4 = listOf(intArrayOf(-1, 19, 10, 1), intArrayOf(1,4,0,10),
        intArrayOf(-1, 3, 10, 22), intArrayOf(1,5,0,10), intArrayOf(-1, 5, 10, 26))

    //should be 3 snps, 27 positions
    counts = compareRangeMaps(makeRangeMap(variantMap, varList3), makeRangeMap(variantMap, varList4), variantMap)
    println("Snp count: ${counts.first}, position count: ${counts.second}")

    //should be 0,30
    counts = compareRangeMaps(makeRangeMap(variantMap, varList1), makeRangeMap(variantMap, varList1), variantMap)
    println("Snp count: ${counts.first}, position count: ${counts.second}")

    //should be 0,27
    counts = compareRangeMaps(makeRangeMap(variantMap, varList2), makeRangeMap(variantMap, varList2), variantMap)
    println("Snp count: ${counts.first}, position count: ${counts.second}")

    //should be 0,27
    counts = compareRangeMaps(makeRangeMap(variantMap, varList2), makeRangeMap(variantMap, varList3), variantMap)
    println("Snp count: ${counts.first}, position count: ${counts.second}")

    //should be 3,27
    counts = compareRangeMaps(makeRangeMap(variantMap, varList2), makeRangeMap(variantMap, varList4), variantMap)
    println("Snp count: ${counts.first}, position count: ${counts.second}")

}

fun validateHaplotypeCounts(taxon : String, pathid : Int) {
    //verifies some of the numbers coming from the path summary
    val genoidMap = mapOf("B73_single" to 28, "B73_paired" to 29, "CML247_single" to 30, "CML247_paired" to 31,
    "Oh43_single" to 32, "Oh43_paired" to 33)
    val gameteGrpMap = mapOf("B73" to "2", "CML247" to 6, "Oh43" to 21)

    val singleGenoid = genoidMap["${taxon}_single"]
    val pairedGenoid = genoidMap["${taxon}_paired"]
    val gamgroup = gameteGrpMap[taxon]

    val targetHapids = HashSet<Int>()
    val pathHapids = ArrayList<HashSet<Int>>()

    DBLoadingUtils.connection(false).use { dbConn ->
        dbConn.createStatement().use { st ->
            val queryTarget = "select haplotypes_id from haplotypes where method_id=4 and gamete_grp_id=$gamgroup"
            st.executeQuery(queryTarget).use { rs ->
                while (rs.next()) {
                    targetHapids.add(rs.getInt(1))
                }
            }


            st.executeQuery("select paths_data from paths where method_id=${pathid} and genoid=${singleGenoid}").use { rs ->
                val bytes = rs.getBytes(1)
                pathHapids.add(DBLoadingUtils.decodePathsForMultipleLists(bytes)[0].toHashSet())
            }

            st.executeQuery("select paths_data from paths where method_id=${pathid} and genoid=${pairedGenoid}").use { rs ->
                val bytes = rs.getBytes(1)
                pathHapids.add(DBLoadingUtils.decodePathsForMultipleLists(bytes)[0].toHashSet())
            }
        }
    }

    println("number of target hapids = ${targetHapids.size}")
    println("number of single path hapids = ${pathHapids[0].size}")
    println("number of paired path hapids = ${pathHapids[1].size}")
    val singleHapidsInTarget = targetHapids.intersect(pathHapids[0]).size
    val pairedHapidsInTarget = targetHapids.intersect(pathHapids[1]).size
    println("number of single Path Hapids in target = $singleHapidsInTarget")
    println("number of paired Path Hapids in target = $pairedHapidsInTarget")

    println("finished counting hapids for $taxon")

}

fun howCompressedIsIt() {
    //the data is so compressed that...
    //paths
    val dbConn = DBLoadingUtils.connection(false)
    val sqlPath = "SELECT paths_data FROM paths"
    myLogger.info("executing: $sqlPath")
    val resultPath = dbConn.createStatement().executeQuery(sqlPath)
    var totalSize = 0
    var count = 0
    while (resultPath.next()) {
        val myBytes = resultPath.getBytes(1)
        totalSize += myBytes.size
        if (myBytes.size > 0) count++
    }
    myLogger.info("The average number of bytes for path_data is ${totalSize / count}")
    resultPath.close()

    val sqlRead = "SELECT mapping_data FROM read_mapping"

    val resultRead = dbConn.createStatement().executeQuery(sqlRead)
    totalSize = 0
    count = 0
    while (resultRead.next()) {
        val myBytes = resultRead.getBytes(1)
        totalSize += myBytes.size
        if (myBytes.size > 0) count++
    }
    myLogger.info("The average number of bytes for mapping_data is ${totalSize / count}")
    resultRead.close()
    dbConn.close()
}