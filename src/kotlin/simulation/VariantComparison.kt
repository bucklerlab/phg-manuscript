package simulation

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.api.HaplotypeNode.VariantInfo
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.Taxon
import org.apache.log4j.BasicConfigurator
import java.util.regex.Pattern

private val baseDir = "/workdir"
private val configFile = "${baseDir}/simulation/config.txt"
private val NUC = Pattern.compile("[ACGTR]")

fun main() {
    BasicConfigurator.configure()
    calculateDistance(buildPHG("mummer4","PathMethod20"))
}

// code used to validate variant comparison
fun buildPHG(haplotypeMethod: String, pathMethod: String): HaplotypeGraph {
    val graphDS = HaplotypeGraphBuilderPlugin(null, false)
        .includeSequences(true)
        .includeVariantContexts(true)
        .configFile(configFile)
        .methods(haplotypeMethod)
        .performFunction(null)

    ParameterCache.load(configFile)
    return AddPathsToGraphPlugin()
        .pathMethod(pathMethod)
        .performFunction(graphDS).getData(0).data as HaplotypeGraph
}

fun calculateDistance(phg: HaplotypeGraph) {
    //compare methods for calculating distance by reference range
    for (refrange in phg.referenceRangeList().subList(51,70)) {
        val taxa2rangeMap = loadTaxaRangeMaps(phg, refrange)
        val Oh43RangeMap = taxa2rangeMap[Taxon("Oh43_Assembly")]
        if (Oh43RangeMap == null) {
            println("Oh43 has no range map for chr ${refrange.chromosome().toString()} pos ${refrange.start()}")
            continue
        }
        val B73RangeMap = taxa2rangeMap[Taxon("B73_Assembly")]
        if (B73RangeMap == null) {
            println("B73 has no range map for chr ${refrange.chromosome().toString()} pos ${refrange.start()}")
            continue
        }

        println("Comparing counts for chr ${refrange.chromosome().toString()} pos ${refrange.start()}")
        for (rangeMapEntry in taxa2rangeMap) {
            var counts = distanceFromSNPs(Oh43RangeMap, rangeMapEntry.value)
            println("phg counts for Oh43 to ${rangeMapEntry.key} are ${counts.first}, ${counts.second}")
            counts = distanceFromSNPs(B73RangeMap, rangeMapEntry.value)
            println("phg counts for B73 to ${rangeMapEntry.key} are ${counts.first}, ${counts.second}")
            counts = compareRangeMaps(Oh43RangeMap, rangeMapEntry.value)
            println("simulate counts for Oh43 to ${rangeMapEntry.key} are ${counts.first}, ${counts.second}")
            counts = compareRangeMaps(B73RangeMap, rangeMapEntry.value)
            println("simulate counts for B73 to ${rangeMapEntry.key} are ${counts.first}, ${counts.second}")
        }
    }
}

//from simulate.kt
private fun calculateCountsSimulate(list1: List<VariantInfo>, list2: List<VariantInfo>) {


}

/**
 * rewrite of simulate method to use variantInfos
 * compares variants in two range maps. Each position that is either a reference call or a SNP for both maps is a volid position.
 * If one or the other is an alt call and a SNP, the position counts as a SNP.
 */
fun compareRangeMaps(variantRangeMap1: RangeMap<Int, VariantInfo>, variantRangeMap2: RangeMap<Int, VariantInfo>) : Pair<Int,Int> {
    val span1 = variantRangeMap1.span()
    val span2 = variantRangeMap2.span()
    var currentPos = Math.min(span1.lowerEndpoint(), span2.lowerEndpoint())
    val endPos = Math.max(span1.upperEndpoint(), span2.upperEndpoint())
    var numberPos = 0
    var numberSnps = 0
    //println("currentPos = $currentPos, endPos = $endPos")
    var prevPos = currentPos
    while (currentPos <= endPos) {
        val varInfo1 = variantRangeMap1[currentPos]
        val varInfo2 = variantRangeMap2[currentPos]

        //do not count if either is null
        if (varInfo1 == null || varInfo2 == null) {
            currentPos++
            continue
        }

        if (!varInfo1.isVariant) { //if (varArray1[0] == -1) {
            //first variant is refblock, end = block length + position - 1
            val end1 = varInfo1.end() //varArray1[1] + varArray1[3] - 1
            if (!varInfo2.isVariant) { //(varArray2[0] == -1) {
                //second variant is refblock
                val end2 = varInfo2.end() //varArray2[1] + varArray2[3] - 1
                val blockEnd = Math.min(end1, end2)
                //since both variants are refblocks skip to the first end, count sites and add to number of positions
                numberPos += Math.max(blockEnd - currentPos + 1, 0)
                currentPos = Math.max(blockEnd + 1, currentPos + 1)
            } else {
                if (varInfo2.genotypeString().equals(varInfo2.refAlleleString())) { //(isRef(varArray2)) {
                    //second variant is not in a ref block but has been called ref allele
                    //so advance only one position and add 1 to position count
                    numberPos++
                    currentPos++
                } else if (varInfo2.genotypeString().equals(varInfo2.altAlleleString())) { //(isAlt(varArray2)) {
                    //is variant2 a snp (not an indel), add 1 to positions and snps
                    if (!varInfo2.isIndel) { //(!isIndel(varArray2, variantMap)) {
                        numberPos++
                        currentPos++
                        numberSnps++
                    } else {
                        //variant2 is an indel, skip to next position but do not increment position count
                        currentPos++
                    }
                } else currentPos++
            }
        } else {
            if (!varInfo2.isVariant) { //(varArray2[0] == -1) {
                if (varInfo1.genotypeString().equals(varInfo1.refAlleleString())) { //(isRef(varArray1)) {
                    //first variant is not in a ref block but has been called ref allele
                    //so advance only one position and add 1 to position count
                    numberPos++
                    currentPos++
                } else if (varInfo1.genotypeString().equals(varInfo1.altAlleleString())) { //(isAlt(varArray1)) {
                    //is variant1 a snp (not an indel), add 1 to positions and snps
                    if (!varInfo1.isIndel) { //(!isIndel(varArray1, variantMap)) {
                        numberPos++
                        currentPos++
                        numberSnps++
                    } else {
                        //variant1 is an indel, skip to next position but do not increment position count
                        currentPos++
                    }
                } else currentPos++
            } else {

                if (varInfo1.variantId() != varInfo2.variantId()) { //(varArray1[1] != varArray2[1]) {
                    //neither variant is in refblock, if variantids are different, advance position but do not count
                    currentPos++
                } else {
                    if (!varInfo1.isVariant) { //(isIndel(varArray1, variantMap)) {
                        //both variant ids are the same. If the variant is not a snp, advance position but do not count
                        currentPos++
                    } else {
                        //it is possible for a call to be unknown so cannot assume that isAlt and !isRef are the same
                        //both  can be false
                        val isRef1 = varInfo1.genotypeString().equals(varInfo1.refAlleleString()) //isRef(varArray1)
                        val isRef2 = varInfo2.genotypeString().equals(varInfo2.refAlleleString())//isRef(varArray2)
                        val isAlt1 = varInfo1.genotypeString().equals(varInfo1.altAlleleString())//isAlt(varArray1)
                        val isAlt2 = varInfo2.genotypeString().equals(varInfo2.altAlleleString())//isAlt(varArray2)

                        if ((isRef1 && isRef2) || (isAlt1 && isAlt2)) {
                            //the variant is a snp, if both are ref or alt increment position count and advance 1
                            numberPos++
                        } else if ((isRef1 && isAlt2) || (isAlt1 && isRef2)) {
                            //if one is ref and one is alt increment snp count also
                            numberPos++
                            numberSnps++
                        }
                        currentPos++
                    }
                }
            }
        }
        if (currentPos <= prevPos) {
            println("prevPos = $prevPos, currentPos = $currentPos, quitting")
            return Pair(numberSnps, numberPos)
        }
        prevPos = currentPos
    }
    return Pair(numberSnps, numberPos)
}


//from consensus plugin
private fun loadTaxaRangeMaps(graph: HaplotypeGraph, currentRefRange: ReferenceRange): MutableMap<Taxon, RangeMap<Int, VariantInfo>> {
    val nodes = graph.nodes(currentRefRange)
    val rangeMaps: MutableMap<Taxon, RangeMap<Int, VariantInfo>> = HashMap()
    val sitesInRef: Int = currentRefRange.end() - currentRefRange.start() + 1
    for (node in nodes) {
        val myTaxon = node.taxaList()[0]
        check(myTaxon != null)
        val optInfo = node.variantInfos()
        if (optInfo.isPresent) {
            //calculate taxon coverage
            val sitesCalled = optInfo.get().stream().mapToInt { `var`: VariantInfo -> `var`.length() }.sum()
            val coverage = sitesCalled.toDouble() / sitesInRef

            //add taxon to map if coverage exceeds minTaxaCoverage
            val myRangeMap = rangeMapForTaxon(optInfo.get(), myTaxon.name)
            if (myRangeMap != null) rangeMaps[myTaxon] = myRangeMap
        }
    }
    return rangeMaps
}

private fun rangeMapForTaxon(infoList: List<VariantInfo>, taxonName: String): RangeMap<Int, VariantInfo>? {
    val infoMap = TreeRangeMap.create<Int, VariantInfo>()
    for (info in infoList) {
        //if the genotype call is N do not process it
        if (info.genotypeString() == "N") continue

        //add the new range after removing any potential overlap
        val infoRange = Range.closed(info.start(), info.end())
        infoMap.remove(infoRange)
        infoMap.put(infoRange, info)
    }
    return infoMap
}

fun distanceFromSNPs(rangeMap1: RangeMap<Int, VariantInfo>, rangeMap2: RangeMap<Int, VariantInfo>): Pair<Int,Int> {
    //this method is designed to work with variant calls from GATK/Senteion, which separates insertions and deletions
    //calls generated from mummer alignments should use the method in createDistanceMatrix()

    //compare only positions represented by single nucleotides
    //test only the pos for variants because pos+1 could be covered by a different variant due to GVCF overlaps
    //except that it is okay to process entire ref blocks
    val start1 = rangeMap1.span().lowerEndpoint()
    val start2 = rangeMap2.span().lowerEndpoint()
    val end1 = rangeMap1.span().upperEndpoint()
    val end2 = rangeMap2.span().upperEndpoint()
    if (start1 == null || start2 == null || end1 == null || end2 == null) return Pair(0,0)
    val rangeStart = Math.min(start1,start2)
    val rangeEnd = Math.max(end1, end2)
    var pos: Int = rangeStart
    var totalSites = 0
    var variantSites = 0
    while (pos <= rangeEnd) {
        val var1 = rangeMap1[pos]
        val var2 = rangeMap2[pos]
        if (var1 == null || var2 == null) {
            pos++ //do not add this site to counts
        } else if (var1.isIndel || var2.isIndel) {
            pos++ //do not add this site to counts
        } else if (var1.genotypeString() == "N" || var2.genotypeString() == "N") {
            pos++ //do not add this site to counts
        } else if (var1.isVariant && var2.isVariant) {
            //query position in each variant. If a single nucleotide increment total
            //if nucleotides are unequal increment variantSites
            val geno1 = var1.genotypeString()
            val geno2 = var2.genotypeString()
            if (NUC.matcher(geno1).matches() && NUC.matcher(geno2).matches()) {
                if (geno1 != geno2) variantSites++
                totalSites++
            }
            pos++
        } else if (var1.isVariant) { // && !var2.isVariant
            //var1 is a snp, so just need to check if it is a ref or alt call
            totalSites++
            if (var1.genotypeString() == var1.altAlleleString()) variantSites++
            pos++
        } else if (var2.isVariant) { // && !var1.isVariant
            //same as for var1 is variant
            //var2 is a snp, so just need to check if it is a ref or alt call
            totalSites++
            if (var2.genotypeString() == var2.altAlleleString()) variantSites++
            pos++
        } else { //both are in ref block
            //move to end of shortest ref block and increment totalSites by number of positions moved
            //move pos 1 past the end of the block
            //do not go beyond the end of currentRefRange
            var endpos = Math.min(var1.end(), var2.end())
            endpos = Math.min(endpos, rangeEnd)
            totalSites += endpos - pos + 1
            pos = endpos + 1
        }
    }
    return Pair(variantSites, totalSites)
}