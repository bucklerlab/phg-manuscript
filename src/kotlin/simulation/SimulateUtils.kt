package simulation

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import java.sql.Connection

fun main(args: Array<String>) {
    if (args[0] == "B73Haplotypes") SimulateUtils().compareHapidsB73()
    else println("Doing nothing.")
}

class SimulateUtils {
    val configFile = "/workdir/pjb39/simulation/configR.txt"

    fun compareHapidsB73() {
        //are the hapids the same for consensus B73 as they are for mummer4
        //gamete group id for B73 is

        val consensusMethodList = listOf("CONSENSUS1","CONSENSUS2","CONSENSUS3","CONSENSUS4",
            "CONSENSUS5","CONSENSUS6","CONSENSUS7","CONSENSUS8","CONSENSUS9","CONSENSUS10")

        val B73GenoId = 2
        val mummer4Hapids = getHapidsForTaxonMethod(B73GenoId, "mummer4")
        println("mummer4 has ${mummer4Hapids.size} haplotypes")
        println("--------------------")
        for (methodName in consensusMethodList) {
            val consensusHapids = getHapidsForTaxonMethod(B73GenoId, methodName)
            println("$methodName has ${consensusHapids.size} haplotypes")
            val intersection = mummer4Hapids.intersect(consensusHapids)
            println("The intersection has ${intersection.size} haplotypes")
            println("----------------")

        }
    }

    fun getHapidsForTaxonMethod(genoid: Int, methodName: String) : HashSet<Int> {
        val hapidSet = HashSet<Int>()
        val sql = "SELECT haplotypes_id " +
                "FROM haplotypes, gamete_haplotypes, gametes, genotypes, methods " +
                "WHERE haplotypes.method_id=methods.method_id AND methods.name = '$methodName' " +
                "AND haplotypes.gamete_grp_id=gamete_haplotypes.gamete_grp_id " +
                "AND gamete_haplotypes.gameteid=gametes.gameteid " +
                "AND gametes.genoid = $genoid"
        println("Executing: $sql")
        DBLoadingUtils.connection(configFile, false).use {dbConn ->
            dbConn.createStatement().executeQuery(sql).use {
                while (it.next()) hapidSet.add(it.getInt(1))
            }
        }

        return hapidSet
    }


}