# README #


## What is this repository for? ##

* This repository holds work that will contribute to the PHG manuscript
* Documents for the project are in a [Google Doc Folder](https://drive.google.com/open?id=16ftlurBbjkjVa1wJToEVevxvlEjs2JMM)

## Simulation ##

Simulation components are coded in SimulateKT. The small jar file created from building this project should be added 
to the tassel-5-standalone lib directory and run using run_anything.pl.

## Simulation Workflow ##

1. Consensus
    * mxDiv in (1e-3, 1e-4, 1e-5, 1e-6, 1e-7), maxNodes in (30), mode in (upgma_assembly, kner_assembly)
    * Output - haplotypes with method = ConsensusN, where N is consecutive integers
1. Pangenome index - for mummer4 and each of the consensi, k=21,w=11
    * Output - stored in pangenomes directory
    * haplotype method will be part of the file name
1. Simulated reads - from mummer4 haplotypes, line in (B73, CML247, Oh43), pairedEnd in (true, false), coverage = 0.1x
    * Output - fastq files with line name, PE or SR, and pt1X (for 0.1X coverage) in the file name
    * Stored in the fastq directory
1. Keyfile - create key file for lines to be analyzed with haplotype method as flow cell
    * Stored in the fastq directory
1. Mapping - different scenarios
    * all methods at 0.25 maxRefRangeErr, f1000,5000, max
    * fParam f5000,6000; f10000,f11000; f15000,16000; f20000,21000; f25000,26000 mummer4, CONSENSUS4 (kmer, 1e-4), CONSENSUS15 (upgma, 1e-4)
    * best fParam for single with paired reads, mummer4
    * subset of previously tested fParam values against consensus (1000,5000; ?), single
    * maxRefRangeErr in 0.05, 0.1, 0.25, 0.5 (mummer4 only)
    * use ReadMapping1, ReadMapping2, ... for methods names
1. Path keyfile - each mapping run will produce a path key file which must be uniquely named to serve as one of the inputs to path finding
1. Path finding - should be run for all reference ranges and for interRegionRanges (genic) only
    * minTaxa (1, 10, 20)
    * minReads (0,1)
    * algorithm type (classic, efficient)
    * splitProb?
    * probCorrect?
    * all mapping and read scenarios with minTaxa=10, minReads=1, other defaults
    * other mapping parameter variants on a subset of scenarios to be determined
    * use a different path method for each unique set of path finding parameters
    * Output - paths stored in the phg db
    * Genic regions vs all

## Analysis of Results ##

All of the analysis code is either in simulate.kt in the src/simulation folder or in R-markdown (Rmd) files in the notebooks folder. 
For each simulated reads file, report number of reads. For each read mapping run, report number of reads mapped and number of reads mapped correctly.
For each path, report proportion of haplotypes correct. Read mapping and path summaries should be both for all reference ranges and for genic ranges only.

## Commands used to run the analysis of assembly taxa - B73, CML247, Oh43 ##

The following commands were run on a Biohpc server managed by the Cornell Institute of Biotechnology Bioinformatics Facility.
Those servers use docker1 commands to wrap docker for security reasons. 

* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt consensus &> /workdir/pjb39/logs/consensus_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt pangenome &> /workdir/pjb39/logs/pangenome_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt reads &> /workdir/pjb39/logs/reads_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt map &> /workdir/pjb39/logs/map_scenario_1_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt mapFParam &> /workdir/pjb39/logs/map_fparam_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt mapMaxErr &> /workdir/pjb39/logs/map_maxerr_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt mappingSummary /workdir/simulation/output/mapping_summary.txt  &> /workdir/pjb39/logs/map_summary_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt mappingByMethodSummary /workdir/simulation/output/mapping_summary  &> /workdir/pjb39/logs/map_summary_rangeMethod_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt findPaths  &> /workdir/pjb39/logs/find_paths_log.txt
* docker1 run -t --rm biohpc_pjb39/simulation /tassel-5-standalone/run_anything.pl -Xmx100G -debug simulation.SimulateKt pathSummary /workdir/simulation/output/pathSummaryAll.txt > /workdir/pjb39/logs/pathSummaryAll_log.txt

## Metrics

* Read mapping
    * number of reads mapped
    * number, proportion of reads mapped correctly
* Path finding
    * proportion of haplotypes in path
    * proportion of haplotypes correct
    * proportion of SNPs called correctly
    
## Software version

docker1 pull maizegenetics/phg:0.0.26

## Database used for analysis

phg_v5Assemblies_20200608.db.  Directions to download at the [Buckler Lab Website](https://www.maizegenetics.net/post/the-first-maize-phg-database-now-available)
This version is older than phg:0.0.26, which means the Liquibase update must be run on it. The command is `docker1 run -t --rm <image tag> /tassel-5-standalone/run_pipeline.pl -Xmx100G -debug -configParameters <path to config file> \
-CheckDBVersionPlugin -outputDir <path to output directory> -endPlugin \
-LiquibaseUpdatePlugin -outputDir <path to output directory> -endPlugin > <path to log file>
`

The maize B73 reference genome used in the consensus step was downloaded from [MaizeGDB](https://www.maizegdb.org/genome/assembly/Zm-B73-REFERENCE-NAM-5.0)

## Who do I talk to? ##

* Repo owner is Peter Bradbury
